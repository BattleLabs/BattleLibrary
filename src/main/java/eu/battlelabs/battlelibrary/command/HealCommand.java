package eu.battlelabs.battlelibrary.command;

import eu.battlelabs.battlelibrary.constant.Permissions;
import eu.battlelabs.battlelibrary.constant.TextType;
import eu.battlelabs.battlelibrary.objects.data.ServerData;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HealCommand implements CommandExecutor {

    private String usage = "/heal {Spieler}";

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("heal")) {
            if (cs instanceof Player) {
                Player player = (Player)cs;

                if (player.hasPermission(Permissions.HEAL_MAIN.getPermission())) {
                    if (args.length == 0) {
                        player.setHealth(player.getHealthScale());
                        player.sendMessage(TextType.PREFIX.getText() + "Du wurdest geheilt.");
                        player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                    } else if (args.length == 1) {
                        if (!player.hasPermission(Permissions.HEAL_OTHER.getPermission())) {
                            player.sendMessage(TextType.NO_PERMISSION.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        if (Bukkit.getPlayer(args[0]) == null) {
                            player.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        Player target = Bukkit.getPlayer(args[0]);

                        target.setHealth(target.getHealthScale());
                        target.sendMessage(TextType.PREFIX.getText() + "Du wurdest geheilt.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        player.sendMessage(TextType.PREFIX.getText() + "" + ServerData.playerdatas.get(target).getRank().getColor() + " §7wurde geheilt.");
                        player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);

                    } else {
                        player.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                    }
                } else {
                    player.sendMessage(TextType.NO_PERMISSION.getText());
                    player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                }
            } else {
                if (args.length == 1) {
                    if (Bukkit.getPlayer(args[0]) == null) {
                        cs.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                        return true;
                    }

                    Player target = Bukkit.getPlayer(args[0]);

                    target.setHealth(target.getHealthScale());
                    target.sendMessage(TextType.PREFIX.getText() + "Du wurdest geheilt.");
                    target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                    cs.sendMessage(TextType.PREFIX.getText() + "" + ServerData.playerdatas.get(target).getRank().getColor() + " §7wurde geheilt.");

                } else {
                    cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }
            }
        }

        return true;
    }
}
