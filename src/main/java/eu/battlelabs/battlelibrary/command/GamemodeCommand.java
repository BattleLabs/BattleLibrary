package eu.battlelabs.battlelibrary.command;

import eu.battlelabs.battlelibrary.constant.Permissions;
import eu.battlelabs.battlelibrary.constant.TextType;
import eu.battlelabs.battlelibrary.objects.data.ServerData;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GamemodeCommand implements CommandExecutor {

    private String usage = "/gamemode {Spieler} <0-3> - /gamemode <0-3>";

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("gamemode")) {
            if (cs instanceof Player) {
                Player player = (Player) cs;

                if (player.hasPermission(Permissions.GAMEMODE_MAIN.getPermission())) {
                    if (args.length == 1) {
                        String gamemode = args[0];

                        if (gamemode.equalsIgnoreCase("survival") || gamemode.equalsIgnoreCase("0")) {
                            player.setGameMode(GameMode.SURVIVAL);
                            player.sendMessage(TextType.PREFIX.getText() + "Du bist nun im §bSurvival Mode§7.");
                            player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                        } else if (gamemode.equalsIgnoreCase("creative") || gamemode.equalsIgnoreCase("1")) {
                            player.setGameMode(GameMode.CREATIVE);
                            player.sendMessage(TextType.PREFIX.getText() + "Du bist nun im §bCreative Mode§7.");
                            player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                        } else if (gamemode.equalsIgnoreCase("adventure") || gamemode.equalsIgnoreCase("2")) {
                            player.setGameMode(GameMode.ADVENTURE);
                            player.sendMessage(TextType.PREFIX.getText() + "Du bist nun im §bAdventure Mode§7.");
                            player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                        } else if (gamemode.equalsIgnoreCase("spectator") || gamemode.equalsIgnoreCase("3")) {
                            player.setGameMode(GameMode.SPECTATOR);
                            player.sendMessage(TextType.PREFIX.getText() + "Du bist nun im §bSpectator Mode§7.");
                            player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                        } else {
                            player.sendMessage(TextType.PREFIX + "§cDieser Gamemode existiert nicht.");
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                        }
                    } else if (args.length == 2) {
                        if (!player.hasPermission(Permissions.GAMEMODE_OTHER.getPermission())) {
                            player.sendMessage(TextType.NO_PERMISSION.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        if (Bukkit.getPlayer(args[1]) == null) {
                            player.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        String gamemode = args[0];
                        Player target = Bukkit.getPlayer(args[1]);

                        if (gamemode.equalsIgnoreCase("survival") || gamemode.equalsIgnoreCase("0")) {
                            target.setGameMode(GameMode.SURVIVAL);
                            target.sendMessage(TextType.PREFIX.getText() + "Du bist nun im §bSurvival Mode§7.");
                            target.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);

                            player.sendMessage(TextType.PREFIX.getText() + "" + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + "§7 ist nun im §bSurvival Mode§7.");
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                        } else if (gamemode.equalsIgnoreCase("creative") || gamemode.equalsIgnoreCase("1")) {
                            target.setGameMode(GameMode.CREATIVE);
                            target.sendMessage(TextType.PREFIX.getText() + "Du bist nun im §bCreative Mode§7.");
                            target.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);

                            player.sendMessage(TextType.PREFIX.getText() + "" + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + "§7 ist nun im §bCreative Mode§7.");
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                        } else if (gamemode.equalsIgnoreCase("adventure") || gamemode.equalsIgnoreCase("2")) {
                            target.setGameMode(GameMode.ADVENTURE);
                            target.sendMessage(TextType.PREFIX.getText() + "Du bist nun im §bAdventure Mode§7.");
                            target.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);

                            player.sendMessage(TextType.PREFIX.getText() + "" + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + "§7 ist nun im §bAdventure Mode§7.");
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                        } else if (gamemode.equalsIgnoreCase("spectator") || gamemode.equalsIgnoreCase("3")) {
                            target.setGameMode(GameMode.SPECTATOR);
                            target.sendMessage(TextType.PREFIX.getText() + "Du bist nun im §bSpectator Mode§7.");
                            target.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);

                            player.sendMessage(TextType.PREFIX.getText() + "" + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + "§7 ist nun im §bSpectator Mode§7.");
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                        } else {
                            player.sendMessage(TextType.PREFIX + "§cDieser Gamemode existiert nicht.");
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                        }
                    } else {
                        player.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                    }
                } else {
                    player.sendMessage(TextType.NO_PERMISSION.getText());
                    player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                }

            } else {
                if (args.length == 2) {
                    if (Bukkit.getPlayer(args[1]) == null) {
                        cs.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                        return true;
                    }

                    String gamemode = args[0];
                    Player target = Bukkit.getPlayer(args[1]);

                    if (gamemode.equalsIgnoreCase("survival") || gamemode.equalsIgnoreCase("0")) {
                        target.setGameMode(GameMode.SURVIVAL);
                        target.sendMessage(TextType.PREFIX.getText() + "Du bist nun im §bSurvival Mode§7.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        cs.sendMessage(TextType.PREFIX.getText() + "" + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + "§7 ist nun im §bSurvival Mode§7.");
                    } else if (gamemode.equalsIgnoreCase("creative") || gamemode.equalsIgnoreCase("1")) {
                        target.setGameMode(GameMode.CREATIVE);
                        target.sendMessage(TextType.PREFIX.getText() + "Du bist nun im §bCreative Mode§7.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        cs.sendMessage(TextType.PREFIX.getText() + "" + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + "§7 ist nun im §bCreative Mode§7.");
                    } else if (gamemode.equalsIgnoreCase("adventure") || gamemode.equalsIgnoreCase("2")) {
                        target.setGameMode(GameMode.ADVENTURE);
                        target.sendMessage(TextType.PREFIX.getText() + "Du bist nun im §bAdventure Mode§7.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        cs.sendMessage(TextType.PREFIX.getText() + "" + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + "§7 ist nun im §bAdventure Mode§7.");
                    } else if (gamemode.equalsIgnoreCase("spectator") || gamemode.equalsIgnoreCase("3")) {
                        target.setGameMode(GameMode.SPECTATOR);
                        target.sendMessage(TextType.PREFIX.getText() + "Du bist nun im §bSpectator Mode§7.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        cs.sendMessage(TextType.PREFIX.getText() + "" + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + "§7 ist nun im §bSpectator Mode§7.");
                    } else {
                        cs.sendMessage(TextType.PREFIX + "§cDieser Gamemode existiert nicht.");
                    }
                } else {
                    cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }

            }
        }

        return true;
    }
}
