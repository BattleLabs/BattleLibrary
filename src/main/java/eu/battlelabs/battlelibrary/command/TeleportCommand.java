package eu.battlelabs.battlelibrary.command;

import eu.battlelabs.battlelibrary.constant.ObjectType;
import eu.battlelabs.battlelibrary.constant.Permissions;
import eu.battlelabs.battlelibrary.constant.TextType;
import eu.battlelabs.battlelibrary.objects.data.ServerData;
import eu.battlelabs.battlelibrary.objects.unit.BattleUser;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeleportCommand implements CommandExecutor {

    private String usage = "/teleport {Spieler} - /teleport {Spieler} {Spieler} - /teleport <x> <y> <z>";

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("teleport")) {
            if (cs instanceof Player) {
                Player player = (Player)cs;
                BattleUser battleUser = ServerData.playerdatas.get(player);

                if (player.hasPermission(Permissions.TELEPORTALL_MAIN.getPermission())) {
                    if (args.length == 1) {
                        if (Bukkit.getPlayer(args[0]) == null) {
                            player.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        Player target = Bukkit.getPlayer(args[0]);

                        player.teleport(target.getLocation());
                        player.sendMessage(TextType.PREFIX.getText() + "Du wurdest zu " + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + " §7teleportiert.");
                        player.playSound(player.getLocation(), Sound.ENDERMAN_TELEPORT, 1, 1);
                    } else if (args.length == 2) {
                        if (!player.hasPermission(Permissions.TELEPORT_OTHER.getPermission())) {
                            player.sendMessage(TextType.NO_PERMISSION.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        if (Bukkit.getPlayer(args[0]) == null || Bukkit.getPlayer(args[1]) == null) {
                            player.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        Player target0 = Bukkit.getPlayer(args[0]);
                        Player target1 = Bukkit.getPlayer(args[1]);

                        target0.teleport(target1.getLocation());
                        target0.sendMessage(TextType.PREFIX.getText() + "Du wurdest zu " + ServerData.playerdatas.get(target1).getRank().getColor() + target1.getName() + " §7teleportiert.");
                        target0.playSound(target0.getLocation(), Sound.ENDERMAN_TELEPORT, 1, 1);

                        player.sendMessage(TextType.PREFIX.getText() + ServerData.playerdatas.get(target0).getRank().getColor() + target0.getName() + " §7wurde zu " + ServerData.playerdatas.get(target1).getRank().getColor() + target1.getName() + " §7teleportiert.");
                        player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                    } else if (args.length == 3) {
                        if (!player.hasPermission(Permissions.TELEPORT_COORD.getPermission())) {
                            player.sendMessage(TextType.NO_PERMISSION.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        if (!ObjectType.isInteger(args[0]) && !ObjectType.isInteger(args[1]) && !ObjectType.isInteger(args[2])) {
                            player.sendMessage(TextType.PREFIX.getText() + "§cX-Y-Z müssen ganze Zahlen sein.");
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        int x = Integer.valueOf(args[0]);
                        int y = Integer.valueOf(args[0]);
                        int z = Integer.valueOf(args[0]);

                        player.teleport(new Location(player.getWorld(), x, y, z));
                        player.sendMessage(TextType.PREFIX.getText() + "Du wurdest zu den angegebenen Koordinaten teleportiert.");
                        player.playSound(player.getLocation(), Sound.ENDERMAN_TELEPORT, 1, 1);
                    } else {
                        player.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                    }
                } else {
                    player.sendMessage(TextType.NO_PERMISSION.getText());
                    player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                }
            } else {
                if (args.length == 2) {
                    if (Bukkit.getPlayer(args[0]) == null || Bukkit.getPlayer(args[1]) == null) {
                        cs.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                        return true;
                    }

                    Player target0 = Bukkit.getPlayer(args[0]);
                    Player target1 = Bukkit.getPlayer(args[1]);

                    target0.teleport(target1.getLocation());
                    target0.sendMessage(TextType.PREFIX.getText() + "Du wurdest zu " + ServerData.playerdatas.get(target1).getRank().getColor() + target1.getName() + " §7teleportiert.");
                    target0.playSound(target0.getLocation(), Sound.ENDERMAN_TELEPORT, 1, 1);

                    cs.sendMessage(TextType.PREFIX.getText() + ServerData.playerdatas.get(target0).getRank().getColor() + target0.getName() + " §7wurde zu " + ServerData.playerdatas.get(target1).getRank().getColor() + target1.getName() + " §7teleportiert.");
                } else {
                    cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }
            }
        }

        return true;
    }
}
