package eu.battlelabs.battlelibrary.command;

import eu.battlelabs.battlelibrary.constant.Permissions;
import eu.battlelabs.battlelibrary.constant.TextType;
import eu.battlelabs.battlelibrary.objects.player.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BroadcastCommand implements CommandExecutor {

    private String usage = "/broadcast <Nachricht>";

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("broadcast")) {
            if (cs instanceof Player) {
                Player player = (Player)cs;

                if (player.hasPermission(Permissions.BROADCAST_MAIN.getPermission())) {
                    if (args.length >= 1) {
                        String msg = "";

                        for (int i = 0; i < args.length; i++) {
                            msg = msg + args[i] + " ";
                        }

                        Bukkit.broadcastMessage(TextType.PREFIX.getText() + ChatColor.translateAlternateColorCodes('&', msg));
                        Messaging.playSoundForAll(Sound.WOOD_CLICK);

                        msg = "";
                    } else {
                        player.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                    }
                } else {
                    player.sendMessage(TextType.NO_PERMISSION.getText());
                    player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                }
            } else {
                if (args.length >= 1) {
                    String msg = "";

                    for (int i = 0; i < args.length; i++) {
                        msg = msg + args[i] + " ";
                    }

                    Bukkit.broadcastMessage(TextType.PREFIX.getText() + ChatColor.translateAlternateColorCodes('&', msg));
                    Messaging.playSoundForAll(Sound.WOOD_CLICK);

                    msg = "";
                } else {
                    cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }
            }
        }

        return true;
    }
}
