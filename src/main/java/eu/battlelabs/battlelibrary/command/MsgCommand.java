package eu.battlelabs.battlelibrary.command;

import eu.battlelabs.battlelibrary.constant.Permissions;
import eu.battlelabs.battlelibrary.constant.TextType;
import eu.battlelabs.battlelibrary.objects.data.ServerData;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;

public class MsgCommand implements CommandExecutor {

    private String usage_msg = "/msg {Spieler} <Nachricht>";
    private String usage_re = "/re <Nachricht>";
    private String usage_socialspy = "/socialspy {Spieler>";

    public static HashMap<Player, Player> re = new HashMap<>();
    public static ArrayList<Player> socialspy = new ArrayList<>();

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("msg")) {
            if (cs instanceof Player) {
                Player player = (Player)cs;

                if (args.length >= 2) {
                    if (Bukkit.getPlayer(args[0]) == null) {
                        player.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                        player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                        return true;
                    }

                    if (player.getName().equals(args[0])) {
                        player.sendMessage(TextType.PREFIX.getText() + "§cDu kannst dir nicht selber schreiben.");
                        player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1 , 1);
                        return true;
                    }

                    Player target = Bukkit.getPlayer(args[0]);
                    String msg = "";

                    for (int i = 1; i < args.length; i++) {
                        msg = msg + args[i] + " ";
                    }

                    player.sendMessage("§8[§7§oIch §8» " + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + "§8] §b" + msg);
                    target.sendMessage("§8[" + ServerData.playerdatas.get(player).getRank().getColor() + player.getName() + " §8» §7§odir§8] §b" + msg);
                    re.put(player, target);
                    re.put(target, player);

                    for (Player socialPlayers : Bukkit.getOnlinePlayers()) {
                        socialPlayers.sendMessage("§8[§cSocialSpy§8] " + ServerData.playerdatas.get(player).getRank().getColor() + player.getName() + " §8> " + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + " §8» §c" + msg);
                    }
                } else {
                    player.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage_msg);
                }
            } else {
                cs.sendMessage("§cNur für Spieler!");
            }
        }

        if (cmd.getName().equalsIgnoreCase("re")) {
            if (cs instanceof Player) {
                Player player = (Player)cs;

                if (args.length >= 1) {
                    if (!re.containsKey(player)) {
                        player.sendMessage(TextType.PREFIX.getText() + "§cDu hast keine private Nachricht verschickt oder bekommen.");
                        player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                        return true;
                    }

                    if (re.get(player) == null) {
                        player.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                        player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                        return true;
                    }

                    Player target = re.get(player);
                    String msg = "";

                    for (int i = 0; i < args.length; i++) {
                        msg = msg + " " + args[i];
                    }

                    player.sendMessage("§8[§7§oIch §8» " + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + "§8] §b" + msg);
                    target.sendMessage("§8[" + ServerData.playerdatas.get(player).getRank().getColor() + player.getName() + " §8» §7§odir§8] §b" + msg);

                    re.put(target, player);

                    for (Player socialPlayers : Bukkit.getOnlinePlayers()) {
                        socialPlayers.sendMessage("§8[§cSocialSpy§8] " + ServerData.playerdatas.get(player).getRank().getColor() + player.getName() + " §8> " + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + " §8» §c" + msg);
                    }
                } else {
                    player.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage_re);
                }
            } else {
                cs.sendMessage("§cNur für Spieler!");
            }
        }

        if (cmd.getName().equalsIgnoreCase("socialspy")) {
            if (cs instanceof Player) {
                Player player = (Player)cs;

                if (player.hasPermission(Permissions.SOCIALSPY_MAIN.getPermission())) {
                    if (args.length == 0) {
                        if (socialspy.contains(player)) {
                            socialspy.remove(player);

                            player.sendMessage(TextType.PREFIX.getText() + "Du kannst nun nicht mehr private Nachrichten spionieren.");
                            player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                        } else {
                            socialspy.add(player);

                            player.sendMessage(TextType.PREFIX.getText() + "Du kannst nun private Nachrichten spionieren.");
                            player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                        }
                    } else if (args.length == 1) {
                        if (!player.hasPermission(Permissions.SOCIALSPY_OTHER.getPermission())) {
                            player.sendMessage(TextType.NO_PERMISSION.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        if (Bukkit.getPlayer(args[0]) == null) {
                            player.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        Player target = Bukkit.getPlayer(args[0]);

                        if (socialspy.contains(target)) {
                            socialspy.remove(target);

                            target.sendMessage(TextType.PREFIX.getText() + "Du kannst nun nicht mehr private Nachrichten spionieren.");
                            target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                            player.sendMessage(TextType.PREFIX.getText() + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + " §7kann nun nicht mehr private Nachrichten spionieren.");
                            player.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);
                        } else {
                            socialspy.add(target);

                            target.sendMessage(TextType.PREFIX.getText() + "Du kannst nun private Nachrichten spionieren.");
                            target.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);

                            player.sendMessage(TextType.PREFIX.getText() + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + " §7kann nun nicht mehr private Nachrichten spionieren.");
                            player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                        }
                    } else {
                        player.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage_socialspy);
                    }
                } else {
                    player.sendMessage(TextType.NO_PERMISSION.getText());
                    player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                }
            } else {
                if (args.length == 1) {
                    if (Bukkit.getPlayer(args[0]) == null) {
                        cs.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                        return true;
                    }

                    Player target = Bukkit.getPlayer(args[0]);

                    if (socialspy.contains(target)) {
                        socialspy.remove(target);

                        target.sendMessage(TextType.PREFIX.getText() + "Du kannst nun nicht mehr private Nachrichten spionieren.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        cs.sendMessage(TextType.PREFIX.getText() + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + " §7kann nun nicht mehr private Nachrichten spionieren.");
                    } else {
                        socialspy.add(target);

                        target.sendMessage(TextType.PREFIX.getText() + "Du kannst nun private Nachrichten spionieren.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        cs.sendMessage(TextType.PREFIX.getText() + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + " §7kann nun nicht mehr private Nachrichten spionieren.");
                    }
                } else {
                    cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage_socialspy);
                }
            }
        }

        return true;
    }
}
