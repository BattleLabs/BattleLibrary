package eu.battlelabs.battlelibrary.command;

import eu.battlelabs.battlelibrary.constant.ObjectType;
import eu.battlelabs.battlelibrary.constant.Permissions;
import eu.battlelabs.battlelibrary.constant.TextType;
import eu.battlelabs.battlelibrary.objects.data.ServerData;
import eu.battlelabs.battlelibrary.objects.unit.BattleUser;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class VotePointCommand implements CommandExecutor {

    private String usage = "/votepoints {Spieler} <add/remove/set> {Amount}";

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("votepoints")) {
            if (cs instanceof Player) {
                Player player = (Player) cs;

                if (args.length == 0) {
                    player.sendMessage(TextType.HEADER.getText());
                    player.sendMessage("");
                    player.sendMessage("§7VotePoints erhältst du, indem du für den Server votest.");
                    player.sendMessage("§7Wie man votet, findest du unter dem Befehl §b/vote");
                    player.sendMessage("§7Weitere Informationen findest du auf unserer Homepage: §bwww.battlelabs.eu");
                    player.sendMessage("");
                    player.sendMessage("§8>> §7Deine aktuellen VotePoints: §b" + ServerData.playerdatas.get(player).getVotePoints() + " VotePoint(s)");
                    player.sendMessage("");
                    player.sendMessage(TextType.HEADER.getText());
                } else if (args.length == 1) {
                    if (Bukkit.getPlayer(args[0]) == null) {
                        player.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                        player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                        return true;
                    }

                    player.sendMessage(TextType.HEADER.getText());
                    player.sendMessage("");
                    player.sendMessage("§7VotePoints erhältst du, indem du für den Server votest.");
                    player.sendMessage("§7Wie man votet, findest du unter dem Befehl §b/vote");
                    player.sendMessage("§7Weitere Informationen findest du auf unserer Homepage: §bwww.battlelabs.eu");
                    player.sendMessage("");
                    player.sendMessage("§8>> §b" + args[0] + "´s§7 aktuellen VotePoints: §b" + ServerData.playerdatas.get(Bukkit.getPlayer(args[0])).getVotePoints() + " VotePoint(s)");
                    player.sendMessage("");
                    player.sendMessage(TextType.HEADER.getText());
                } else if (args.length == 3) {
                    if (args[1].equalsIgnoreCase("add")) {
                        if (!player.hasPermission(Permissions.VOTEPOINT_ADD.getPermission())) {
                            player.sendMessage(TextType.NO_PERMISSION.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        if (Bukkit.getPlayer(args[0]) == null) {
                            player.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        if (!ObjectType.isInteger(args[2])) {
                            player.sendMessage(TextType.PREFIX.getText() + "§cEs muss eine Zahl sein.");
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        Player target = Bukkit.getPlayer(args[0]);
                        int amount = Integer.valueOf(args[2]);
                        BattleUser battleUser = ServerData.playerdatas.get(target);

                        battleUser.addVotePoints(amount);
                        target.sendMessage(TextType.PREFIX.getText() + "Dir wurden §b" + amount + "VotePoint(s) §7hinzugefügt.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        player.sendMessage(TextType.PREFIX.getText() + battleUser.getRank().getColor() + target.getName() + " §7wurden §b" + amount + " VotePoint(s) §7hinzugefügt.");
                        player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                    } else if (args[1].equalsIgnoreCase("remove")) {
                        if (!player.hasPermission(Permissions.VOTEPOINT_REMOVE.getPermission())) {
                            player.sendMessage(TextType.NO_PERMISSION.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        if (Bukkit.getPlayer(args[0]) == null) {
                            player.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        if (!ObjectType.isInteger(args[2])) {
                            player.sendMessage(TextType.PREFIX.getText() + "§cEs muss eine Zahl sein.");
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        Player target = Bukkit.getPlayer(args[0]);
                        int amount = Integer.valueOf(args[2]);
                        BattleUser battleUser = ServerData.playerdatas.get(target);

                        battleUser.removeVotePoints(amount);
                        target.sendMessage(TextType.PREFIX.getText() + "Dir wurden §b" + amount + "VotePoint(s) §7entzogen.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        player.sendMessage(TextType.PREFIX.getText() + battleUser.getRank().getColor() + target.getName() + " §7wurden §b" + amount + " VotePoint(s) §7entzogen.");
                        player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                    } else if (args[1].equalsIgnoreCase("set")) {
                        if (!player.hasPermission(Permissions.VOTEPOINT_SET.getPermission())) {
                            player.sendMessage(TextType.NO_PERMISSION.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        if (Bukkit.getPlayer(args[0]) == null) {
                            player.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        if (!ObjectType.isInteger(args[2])) {
                            player.sendMessage(TextType.PREFIX.getText() + "§cEs muss eine Zahl sein.");
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        Player target = Bukkit.getPlayer(args[0]);
                        int amount = Integer.valueOf(args[2]);
                        BattleUser battleUser = ServerData.playerdatas.get(target);

                        battleUser.setVotePoints(amount);
                        target.sendMessage(TextType.PREFIX.getText() + "Dir wurden §b" + amount + "VotePoint(s) §7gesetzt.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        player.sendMessage(TextType.PREFIX.getText() + battleUser.getRank().getColor() + target.getName() + " §7wurden §b" + amount + " VotePoint(s) §7gesetzt.");
                        player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                    } else {
                        player.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                    }
                } else {
                    player.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }
            } else {
                if (args.length == 1) {
                    if (Bukkit.getPlayer(args[0]) == null) {
                        cs.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                        return true;
                    }

                    cs.sendMessage(TextType.HEADER.getText());
                    cs.sendMessage("");
                    cs.sendMessage("§7VotePoints erhältst du, indem du für den Server votest.");
                    cs.sendMessage("§7Wie man votet, findest du unter dem Befehl §b/vote");
                    cs.sendMessage("§7Weitere Informationen findest du auf unserer Homepage: §bwww.battlelabs.eu");
                    cs.sendMessage("");
                    cs.sendMessage("§8>> §b" + args[0] + "´s§7 aktuellen VotePoints: §b" + ServerData.playerdatas.get(Bukkit.getPlayer(args[0])).getTokens() + " VotePoint(s)");
                    cs.sendMessage("");
                    cs.sendMessage(TextType.HEADER.getText());
                } else if (args.length == 3) {
                    if (args[1].equalsIgnoreCase("add")) {
                        if (Bukkit.getPlayer(args[0]) == null) {
                            cs.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            return true;
                        }

                        if (!ObjectType.isInteger(args[2])) {
                            cs.sendMessage(TextType.PREFIX.getText() + "§cEs muss eine Zahl sein.");
                            return true;
                        }

                        Player target = Bukkit.getPlayer(args[0]);
                        int amount = Integer.valueOf(args[2]);
                        BattleUser battleUser = ServerData.playerdatas.get(target);

                        battleUser.addVotePoints(amount);
                        target.sendMessage(TextType.PREFIX.getText() + "Dir wurden §b" + amount + "VotePoint(s) §7hinzugefügt.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        cs.sendMessage(TextType.PREFIX.getText() + battleUser.getRank().getColor() + target.getName() + " §7wurden §b" + amount + " VotePoint(s) §7hinzugefügt.");
                    } else if (args[1].equalsIgnoreCase("remove")) {
                        if (Bukkit.getPlayer(args[0]) == null) {
                            cs.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            return true;
                        }

                        if (!ObjectType.isInteger(args[2])) {
                            cs.sendMessage(TextType.PREFIX.getText() + "§cEs muss eine Zahl sein.");
                            return true;
                        }

                        Player target = Bukkit.getPlayer(args[0]);
                        int amount = Integer.valueOf(args[2]);
                        BattleUser battleUser = ServerData.playerdatas.get(target);

                        battleUser.removeVotePoints(amount);
                        target.sendMessage(TextType.PREFIX.getText() + "Dir wurden §b" + amount + "VotePoint(s) §7entzogen.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        cs.sendMessage(TextType.PREFIX.getText() + battleUser.getRank().getColor() + target.getName() + " §7wurden §b" + amount + " VotePoint(s) §7entzogen.");
                    } else if (args[1].equalsIgnoreCase("set")) {
                        if (Bukkit.getPlayer(args[0]) == null) {
                            cs.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            return true;
                        }

                        if (!ObjectType.isInteger(args[2])) {
                            cs.sendMessage(TextType.PREFIX.getText() + "§cEs muss eine Zahl sein.");
                            return true;
                        }

                        Player target = Bukkit.getPlayer(args[0]);
                        int amount = Integer.valueOf(args[2]);
                        BattleUser battleUser = ServerData.playerdatas.get(target);

                        battleUser.setVotePoints(amount);
                        target.sendMessage(TextType.PREFIX.getText() + "Dir wurden §b" + amount + "VotePoint(s) §7gesetzt.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        cs.sendMessage(TextType.PREFIX.getText() + battleUser.getRank().getColor() + target.getName() + " §7wurden §b" + amount + " VotePoint(s) §7gesetzt.");
                    } else {
                        cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                    }
                } else {
                    cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }
            }

        }

        return true;
    }
}
