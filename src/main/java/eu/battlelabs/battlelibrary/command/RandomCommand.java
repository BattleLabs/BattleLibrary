package eu.battlelabs.battlelibrary.command;

import eu.battlelabs.battlelibrary.BattleLibrary;
import eu.battlelabs.battlelibrary.constant.Permissions;
import eu.battlelabs.battlelibrary.constant.TextType;
import eu.battlelabs.battlelibrary.objects.data.ServerData;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.github.paperspigot.Title;

import java.util.Random;

public class RandomCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("random")) {
            if (cs instanceof Player) {
                Player player = (Player)cs;


                if (player.hasPermission(Permissions.RANDOM_MAIN.getPermission())) {
                    new BukkitRunnable() {
                        int i = 0;

                        @Override
                        public void run() {
                            i++;

                            if(i < 10) {
                                Player random = null;
                                Random r = new Random();
                                Object[] ps = Bukkit.getOnlinePlayers().toArray();
                                int i = r.nextInt(ps.length);
                                random = (Player) ps[i];

                                for(Player players : Bukkit.getOnlinePlayers())  {
                                    Title title = new Title(ServerData.getRank(random).getColor() + random.getName());
                                    title.builder().fadeIn(5).stay(10).fadeOut(5);
                                    players.sendTitle(title);
                                    players.playSound(players.getLocation(), Sound.NOTE_PLING, 1, 1);
                                }

                            }else if(i == 11) {
                                Player random = null;
                                Random r = new Random();
                                Object[] ps = Bukkit.getOnlinePlayers().toArray();
                                int i = r.nextInt(ps.length);
                                random = (Player) ps[i];

                                for(Player players : Bukkit.getOnlinePlayers())  {
                                    Title title = new Title(ServerData.getRank(random).getColor() + random.getName() , "§7wurde ausgelost!");
                                    title.builder().fadeIn(20).stay(40).fadeOut(20);
                                    players.playSound(players.getLocation(), Sound.NOTE_PLING, 1, 1);
                                }

                                Bukkit.broadcastMessage("§8§l>> " + ServerData.playerdatas.get(random).getRank().getColor() + random.getName() + " §7wurde zufällig ausgelost!");
                            }
                        }
                    }.runTaskTimerAsynchronously(BattleLibrary.getInstance(), 0L, 10L);
                } else {
                    player.sendMessage(TextType.NO_PERMISSION.getText());
                    player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                }
            } else {
                new BukkitRunnable() {
                    int i = 0;

                    @Override
                    public void run() {
                        i++;

                        if(i < 10) {
                            Player random = null;
                            Random r = new Random();
                            Object[] ps = Bukkit.getOnlinePlayers().toArray();
                            int i = r.nextInt(ps.length);
                            random = (Player) ps[i];

                            for(Player players : Bukkit.getOnlinePlayers())  {
                                Title title = new Title(ServerData.getRank(random).getColor() + random.getName());
                                title.builder().fadeIn(5).stay(10).fadeOut(5);
                                players.sendTitle(title);
                                players.playSound(players.getLocation(), Sound.NOTE_PLING, 1, 1);
                            }

                        }else if(i == 11) {
                            Player random = null;
                            Random r = new Random();
                            Object[] ps = Bukkit.getOnlinePlayers().toArray();
                            int i = r.nextInt(ps.length);
                            random = (Player) ps[i];

                            for(Player players : Bukkit.getOnlinePlayers())  {
                                Title title = new Title(ServerData.getRank(random).getColor() + random.getName() , "§7wurde ausgelost!");
                                title.builder().fadeIn(20).stay(40).fadeOut(20);
                                players.playSound(players.getLocation(), Sound.NOTE_PLING, 1, 1);
                            }

                            Bukkit.broadcastMessage("§8§l>> " + ServerData.playerdatas.get(random).getRank().getColor() + random.getName() + " §7wurde zufällig ausgelost!");
                        }
                    }
                }.runTaskTimerAsynchronously(BattleLibrary.getInstance(), 0L, 10L);
            }
        }

        return true;
    }
}
