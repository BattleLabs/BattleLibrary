package eu.battlelabs.battlelibrary.command;

import eu.battlelabs.battlelibrary.constant.Permissions;
import eu.battlelabs.battlelibrary.constant.TextType;
import eu.battlelabs.battlelibrary.objects.data.ServerData;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.util.ArrayList;

public class GodCommand implements CommandExecutor, Listener {

    private String usage = "/god {Spieler}";

    public static ArrayList<Player> god = new ArrayList<>();

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("god")) {
            if (cs instanceof Player) {
                Player player = (Player) cs;

                if (player.hasPermission(Permissions.GOD_MAIN.getPermission())) {
                    if (args.length == 0) {
                        if (god.contains(player)) {
                            god.remove(player);

                            player.sendMessage(TextType.PREFIX.getText() + "Du bist nun nicht mehr im Gott-Modus.");
                            player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                        } else {
                            god.add(player);

                            player.sendMessage(TextType.PREFIX.getText() + "Du bist nun im Gott-Modus.");
                            player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                        }
                    } else if (args.length == 1) {
                        if (!player.hasPermission(Permissions.GOD_OTHER.getPermission())) {
                            player.sendMessage(TextType.NO_PERMISSION.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        if (Bukkit.getPlayer(args[0]) == null) {
                            player.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        Player target = Bukkit.getPlayer(args[0]);

                        if (god.contains(target)) {
                            god.remove(target);

                            target.sendMessage(TextType.PREFIX.getText() + "Du bist nun nicht mehr im Gott-Modus.");
                            target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                            player.sendMessage(TextType.PREFIX.getText() + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + " §7ist nun nicht mehr im Gott-Modus.");
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                        } else {
                            god.add(target);

                            target.sendMessage(TextType.PREFIX.getText() + "Du bist nun im Gott-Modus.");
                            target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                            player.sendMessage(TextType.PREFIX.getText() + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + " §7ist nun im Gott-Modus.");
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                        }
                    } else {
                        player.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                    }
                } else {
                    player.sendMessage(TextType.NO_PERMISSION.getText());
                    player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                }
            } else {
                if (args.length == 1) {
                    if (Bukkit.getPlayer(args[0]) == null) {
                        cs.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                        return true;
                    }

                    Player target = Bukkit.getPlayer(args[0]);

                    if (god.contains(target)) {
                        god.remove(target);

                        target.sendMessage(TextType.PREFIX.getText() + "Du bist nun nicht mehr im Gott-Modus.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        cs.sendMessage(TextType.PREFIX.getText() + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + " §7ist nun nicht mehr im Gott-Modus.");
                    } else {
                        god.add(target);

                        target.sendMessage(TextType.PREFIX.getText() + "Du bist nun im Gott-Modus.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        cs.sendMessage(TextType.PREFIX.getText() + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + " §7ist nun im Gott-Modus.");
                    }
                } else {
                    cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }
            }

        }
        return true;
    }

    @EventHandler
    public void onDmg(EntityDamageByEntityEvent e) {

        if (e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
            Player player = (Player) e.getEntity();
            Player damager = (Player) e.getDamager();

            if (god.contains(player)) {
                e.setCancelled(true);
            }

            if (god.contains(damager)) {
                if (!damager.hasPermission(Permissions.GOD_CANCEL.getPermission())) {
                    e.setCancelled(true);
                }
            }
        }

    }
}