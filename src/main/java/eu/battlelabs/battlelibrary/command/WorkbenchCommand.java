package eu.battlelabs.battlelibrary.command;

import eu.battlelabs.battlelibrary.constant.Permissions;
import eu.battlelabs.battlelibrary.constant.TextType;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WorkbenchCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("workbench")) {
            if (cs instanceof Player) {
                Player player = (Player)cs;

                if (!player.hasPermission(Permissions.WORKBECNH_MAIN.getPermission())) {
                    player.sendMessage(TextType.NO_PERMISSION.getText());
                    player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                    return true;
                }

                player.openWorkbench(player.getLocation(), true);
                player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
            } else {
                cs.sendMessage("§cNur für Spieler!");
            }
        }

        return true;
    }
}
