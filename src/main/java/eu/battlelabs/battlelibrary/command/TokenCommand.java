package eu.battlelabs.battlelibrary.command;

import eu.battlelabs.battlelibrary.constant.ObjectType;
import eu.battlelabs.battlelibrary.constant.Permissions;
import eu.battlelabs.battlelibrary.constant.TextType;
import eu.battlelabs.battlelibrary.objects.data.ServerData;
import eu.battlelabs.battlelibrary.objects.unit.BattleUser;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TokenCommand implements CommandExecutor {

    private String usage = "/tokens {User} <add/remove/set/> {Anzahl}";

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("tokens")) {
            if (cs instanceof Player) {
                Player player = (Player)cs;

                if (args.length == 0) {
                    player.sendMessage(TextType.HEADER.getText());
                    player.sendMessage("");
                    player.sendMessage("§7Tokens bekommst du, indem du Spieler tötest.");
                    player.sendMessage("§7Je nach Kampfsituation bekommst du unteschiedlich viele Tokens.");
                    player.sendMessage("§7Weitere Informationen findest du auf unserer Homepage: §bwww.battlelabs.eu");
                    player.sendMessage("");
                    player.sendMessage("§8>> §7Deine aktuellen Tokens: §b" + ServerData.playerdatas.get(player).getTokens() + " Token(s)");
                    player.sendMessage("");
                    player.sendMessage(TextType.HEADER.getText());
                } else if (args.length == 1) {
                    if (Bukkit.getPlayer(args[0]) == null) {
                        player.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                        player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                        return true;
                    }

                    player.sendMessage(TextType.HEADER.getText());
                    player.sendMessage("");
                    player.sendMessage("§7Tokens bekommst du, indem du Spieler tötest.");
                    player.sendMessage("§7Je nach Kampfsituation bekommst du unteschiedlich viele Tokens.");
                    player.sendMessage("§7Weitere Informationen findest du auf unserer Homepage: §bwww.battlelabs.eu");
                    player.sendMessage("");
                    player.sendMessage("§8>> §b" + args[0] +"´s§7 aktuellen Tokens: §b" + ServerData.playerdatas.get(Bukkit.getPlayer(args[0])).getTokens() + " Token(s)");
                    player.sendMessage("");
                    player.sendMessage(TextType.HEADER.getText());
                } else if (args.length == 3) {
                    if (args[1].equalsIgnoreCase("add")) {
                        if (!player.hasPermission(Permissions.TOKENS_ADD.getPermission())) {
                            player.sendMessage(TextType.NO_PERMISSION.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        if (Bukkit.getPlayer(args[0]) == null) {
                            player.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        if (!ObjectType.isInteger(args[2])) {
                            player.sendMessage(TextType.PREFIX.getText() + "§cEs muss eine Zahl sein.");
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        Player target = Bukkit.getPlayer(args[0]);
                        int amount = Integer.valueOf(args[2]);
                        BattleUser battleUser = ServerData.playerdatas.get(target);

                        battleUser.addTokens(amount);
                        target.sendMessage(TextType.PREFIX.getText() + "Dir wurden §b" + amount + "Token(s) §7hinzugefügt.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        player.sendMessage(TextType.PREFIX.getText() + battleUser.getRank().getColor() + target.getName() + " §7wurden §b" + amount + " Token(s) §7hinzugefügt.");
                        player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                    } else if (args[1].equalsIgnoreCase("remove")) {
                        if (!player.hasPermission(Permissions.TOKENS_REMOVE.getPermission())) {
                            player.sendMessage(TextType.NO_PERMISSION.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        if (Bukkit.getPlayer(args[0]) == null) {
                            player.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        if (!ObjectType.isInteger(args[2])) {
                            player.sendMessage(TextType.PREFIX.getText() + "§cEs muss eine Zahl sein.");
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        Player target = Bukkit.getPlayer(args[0]);
                        int amount = Integer.valueOf(args[2]);
                        BattleUser battleUser = ServerData.playerdatas.get(target);

                        battleUser.removeTokens(amount);
                        target.sendMessage(TextType.PREFIX.getText() + "Dir wurden §b" + amount + "Token(s) §7entzogen.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        player.sendMessage(TextType.PREFIX.getText() + battleUser.getRank().getColor() + target.getName() + " §7wurden §b" + amount + " Token(s) §7entzogen.");
                        player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                    } else if (args[1].equalsIgnoreCase("set")) {
                        if (!player.hasPermission(Permissions.TOKENS_SET.getPermission())) {
                            player.sendMessage(TextType.NO_PERMISSION.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        if (Bukkit.getPlayer(args[0]) == null) {
                            player.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        if (!ObjectType.isInteger(args[2])) {
                            player.sendMessage(TextType.PREFIX.getText() + "§cEs muss eine Zahl sein.");
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        Player target = Bukkit.getPlayer(args[0]);
                        int amount = Integer.valueOf(args[2]);
                        BattleUser battleUser = ServerData.playerdatas.get(target);

                        battleUser.setTokens(amount);
                        target.sendMessage(TextType.PREFIX.getText() + "Dir wurden §b" + amount + "Token(s) §7gesetzt.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        player.sendMessage(TextType.PREFIX.getText() + battleUser.getRank().getColor() + target.getName() + " §7wurden §b" + amount + " Token(s) §7gesetzt.");
                        player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                    } else {
                        player.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                    }
                } else {
                    player.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }
            } else {
                if (args.length == 1) {
                    if (Bukkit.getPlayer(args[0]) == null) {
                        cs.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                        return true;
                    }

                    cs.sendMessage(TextType.HEADER.getText());
                    cs.sendMessage("");
                    cs.sendMessage("§7Tokens bekommst du, indem du Spieler tötest.");
                    cs.sendMessage("§7Je nach Kampfsituation bekommst du unteschiedlich viele Tokens.");
                    cs.sendMessage("§7Weitere Informationen findest du auf unserer Homepage: §bwww.herolabs.eu");
                    cs.sendMessage("");
                    cs.sendMessage("§8>> §b" + args[0] +"´s§7 aktuellen Tokens: §b" + ServerData.playerdatas.get(Bukkit.getPlayer(args[0])).getTokens() + " Token(s)");
                    cs.sendMessage("");
                    cs.sendMessage(TextType.HEADER.getText());
                } else if (args.length == 3) {
                    if (args[1].equalsIgnoreCase("add")) {
                        if (Bukkit.getPlayer(args[0]) == null) {
                            cs.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            return true;
                        }

                        if (!ObjectType.isInteger(args[2])) {
                            cs.sendMessage(TextType.PREFIX.getText() + "§cEs muss eine Zahl sein.");
                            return true;
                        }

                        Player target = Bukkit.getPlayer(args[0]);
                        int amount = Integer.valueOf(args[2]);
                        BattleUser battleUser = ServerData.playerdatas.get(target);

                        battleUser.addTokens(amount);
                        target.sendMessage(TextType.PREFIX.getText() + "Dir wurden §b" + amount + "Token(s) §7hinzugefügt.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        cs.sendMessage(TextType.PREFIX.getText() + battleUser.getRank().getColor() + target.getName() + " §7wurden §b" + amount + " Token(s) §7hinzugefügt.");
                    } else if (args[1].equalsIgnoreCase("remove")) {
                        if (Bukkit.getPlayer(args[0]) == null) {
                            cs.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            return true;
                        }

                        if (!ObjectType.isInteger(args[2])) {
                            cs.sendMessage(TextType.PREFIX.getText() + "§cEs muss eine Zahl sein.");
                            return true;
                        }

                        Player target = Bukkit.getPlayer(args[0]);
                        int amount = Integer.valueOf(args[2]);
                        BattleUser battleUser = ServerData.playerdatas.get(target);

                        battleUser.removeTokens(amount);
                        target.sendMessage(TextType.PREFIX.getText() + "Dir wurden §b" + amount + "Token(s) §7entzogen.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        cs.sendMessage(TextType.PREFIX.getText() + battleUser.getRank().getColor() + target.getName() + " §7wurden §b" + amount + " Token(s) §7entzogen.");
                    } else if (args[1].equalsIgnoreCase("set")) {
                        if (Bukkit.getPlayer(args[0]) == null) {
                            cs.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            return true;
                        }

                        if (!ObjectType.isInteger(args[2])) {
                            cs.sendMessage(TextType.PREFIX.getText() + "§cEs muss eine Zahl sein.");
                            return true;
                        }

                        Player target = Bukkit.getPlayer(args[0]);
                        int amount = Integer.valueOf(args[2]);
                        BattleUser battleUser = ServerData.playerdatas.get(target);

                        battleUser.setTokens(amount);
                        target.sendMessage(TextType.PREFIX.getText() + "Dir wurden §b" + amount + "Token(s) §7gesetzt.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        cs.sendMessage(TextType.PREFIX.getText() + battleUser.getRank().getColor() + target.getName() + " §7wurden §b" + amount + " Token(s) §7gesetzt.");
                    } else {
                        cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                    }
                } else {
                    cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }
            }
        }

        return true;
    }
}
