package eu.battlelabs.battlelibrary.command;

import eu.battlelabs.battlelibrary.BattleLibrary;
import eu.battlelabs.battlelibrary.constant.Permissions;
import eu.battlelabs.battlelibrary.constant.TextType;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GlobalmuteCommand implements CommandExecutor {

    private String usage = "/globalmute toggle";

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("globalmute")) {
            if (cs instanceof Player) {
                Player player = (Player)cs;

                if (player.hasPermission(Permissions.GLOBALMUTE_MAIN.getPermission())) {
                    if (args.length == 0) {
                        if (BattleLibrary.chat) {
                            player.sendMessage(TextType.HEADER.getText());
                            player.sendMessage("");
                            player.sendMessage("§7Der Chat ist §aaktiviert.");
                            if (player.hasPermission(Permissions.GLOBALMUTE_TOGGLE.getPermission())) {
                                player.sendMessage("§7Du kannst du den Chat mit §b/globalmute toggle §cdeaktivieren§7.");
                            }
                            player.sendMessage("");
                            player.sendMessage(TextType.HEADER.getText());
                        } else{
                            player.sendMessage(TextType.HEADER.getText());
                            player.sendMessage("");
                            player.sendMessage("§7Der Chat ist §cdeaktiviert.");
                            if (player.hasPermission(Permissions.GLOBALMUTE_TOGGLE.getPermission())) {
                                player.sendMessage("§7Du kannst du den Chat mit §b/globalmute toggle §aktivieren§7.");
                            }
                            player.sendMessage("");
                            player.sendMessage(TextType.HEADER.getText());
                        }
                    } else if (args.length == 1) {
                        if (args[0].equalsIgnoreCase("toggle")) {
                            if (!player.hasPermission(Permissions.GLOBALMUTE_TOGGLE.getPermission())) {
                                player.sendMessage(TextType.NO_PERMISSION.getText());
                                player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                                return true;
                            }

                            if (BattleLibrary.chat) {
                                BattleLibrary.chat = false;

                                new Thread() {
                                    @Override
                                    public void run() {
                                        for (int i = 0; i < 100; i++) {
                                            for (Player players : Bukkit.getOnlinePlayers()) {
                                                if (!players.hasPermission(Permissions.CHATCLEAR_IGNORE.getPermission())) {
                                                    players.sendMessage("");
                                                }
                                            }
                                        }

                                        super.run();
                                    }
                                }.start();

                                Bukkit.broadcastMessage("");
                                Bukkit.broadcastMessage("");
                                Bukkit.broadcastMessage("§8>>      §7Der §bGlobale Chat §7wurde §c§ldeaktiviert§7!");
                                Bukkit.broadcastMessage("");
                                Bukkit.broadcastMessage("");
                            } else {
                                BattleLibrary.chat = true;

                                new Thread() {
                                    @Override
                                    public void run() {
                                        for (int i = 0; i < 100; i++) {
                                            for (Player players : Bukkit.getOnlinePlayers()) {
                                                if (!players.hasPermission(Permissions.CHATCLEAR_IGNORE.getPermission())) {
                                                    players.sendMessage("");
                                                }
                                            }
                                        }

                                        super.run();
                                    }
                                }.start();

                                Bukkit.broadcastMessage("");
                                Bukkit.broadcastMessage("");
                                Bukkit.broadcastMessage("§8>>      §7Der §bGlobale Chat §7wurde §a§laktiviert§7!");
                                Bukkit.broadcastMessage("");
                                Bukkit.broadcastMessage("");
                            }
                        } else {
                            player.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                        }
                    } else {
                        player.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                    }
                } else {
                    player.sendMessage(TextType.NO_PERMISSION.getText());
                    player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                }
            } else {
                if (args.length == 0) {
                    if (BattleLibrary.chat) {
                        cs.sendMessage(TextType.HEADER.getText());
                        cs.sendMessage("");
                        cs.sendMessage("§7Der Chat ist §aaktiviert.");
                        cs.sendMessage("§7Du kannst du den Chat mit §b/globalmute toggle §cdeaktivieren§7.");
                        cs.sendMessage("");
                        cs.sendMessage(TextType.HEADER.getText());
                    } else{
                        cs.sendMessage(TextType.HEADER.getText());
                        cs.sendMessage("");
                        cs.sendMessage("§7Der Chat ist §cdeaktiviert.");
                        cs.sendMessage("§7Du kannst du den Chat mit §b/globalmute toggle §aktivieren§7.");
                        cs.sendMessage("");
                        cs.sendMessage(TextType.HEADER.getText());
                    }
                } else if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("toggle")) {
                        if (BattleLibrary.chat) {
                            BattleLibrary.chat = false;

                            new Thread() {
                                @Override
                                public void run() {
                                    for (int i = 0; i < 100; i++) {
                                        for (Player players : Bukkit.getOnlinePlayers()) {
                                            if (!players.hasPermission(Permissions.CHATCLEAR_IGNORE.getPermission())) {
                                                players.sendMessage("");
                                            }
                                        }
                                    }

                                    super.run();
                                }
                            }.start();

                            Bukkit.broadcastMessage("");
                            Bukkit.broadcastMessage("");
                            Bukkit.broadcastMessage("§8>>      §7Der §bGlobale Chat §7wurde §c§ldeaktiviert§7!");
                            Bukkit.broadcastMessage("");
                            Bukkit.broadcastMessage("");
                        } else {
                            BattleLibrary.chat = true;

                            new Thread() {
                                @Override
                                public void run() {
                                    for (int i = 0; i < 100; i++) {
                                        for (Player players : Bukkit.getOnlinePlayers()) {
                                            if (!players.hasPermission(Permissions.CHATCLEAR_IGNORE.getPermission())) {
                                                players.sendMessage("");
                                            }
                                        }
                                    }

                                    super.run();
                                }
                            }.start();

                            Bukkit.broadcastMessage("");
                            Bukkit.broadcastMessage("");
                            Bukkit.broadcastMessage("§8>>      §7Der §bGlobale Chat §7wurde §a§laktiviert§7!");
                            Bukkit.broadcastMessage("");
                            Bukkit.broadcastMessage("");
                        }
                    } else {
                        cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                    }
                } else {
                    cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }
            }
        }

        return true;
    }
}
