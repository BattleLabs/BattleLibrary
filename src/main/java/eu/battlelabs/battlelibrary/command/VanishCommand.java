package eu.battlelabs.battlelibrary.command;

import eu.battlelabs.battlelibrary.constant.Permissions;
import eu.battlelabs.battlelibrary.constant.TextType;
import eu.battlelabs.battlelibrary.objects.data.ServerData;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class VanishCommand implements CommandExecutor {

    private String usage = "/vanish {Player}";

    public static ArrayList<Player> vanish = new ArrayList<>();

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("vanish")) {
            if (cs instanceof Player) {
                Player player = (Player) cs;

                if (player.hasPermission(Permissions.VANISH_MAIN.getPermission())) {
                    if (args.length == 0) {
                        if (vanish.contains(player)) {
                            vanish.remove(player);

                            for (Player players : Bukkit.getOnlinePlayers()) {
                                players.showPlayer(player);
                            }

                            player.sendMessage(TextType.PREFIX.getText() + "Du bist nun nicht mehr unsichtbar.");
                            player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                        } else {
                            vanish.add(player);

                            for (Player players : Bukkit.getOnlinePlayers()) {
                                if (!players.hasPermission(Permissions.VANISH_IGNORE.getPermission())) {
                                    players.hidePlayer(player);
                                }
                            }

                            player.sendMessage(TextType.PREFIX.getText() + "Du bist nun unsichtbar.");
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                        }
                    } else if (args.length == 1) {
                        if (!player.hasPermission(Permissions.VANISH_OTHER.getPermission())) {
                            player.sendMessage(TextType.NO_PERMISSION.getText());
                            return true;
                        }

                        if (Bukkit.getPlayer(args[0]) == null) {
                            player.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        Player target = Bukkit.getPlayer(args[0]);

                        if (vanish.contains(target)) {
                            vanish.remove(target);

                            for (Player players : Bukkit.getOnlinePlayers()) {
                                players.showPlayer(target);
                            }

                            target.sendMessage(TextType.PREFIX.getText() + "Du bist nun nicht mehr unsichtbar.");
                            target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                            player.sendMessage(TextType.PREFIX.getText() + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + " ist nun nicht mehr unsichtbar.");
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                        } else {
                            vanish.add(target);

                            for (Player players : Bukkit.getOnlinePlayers()) {
                                if (!players.hasPermission(Permissions.VANISH_IGNORE.getPermission())) {
                                    players.hidePlayer(target);
                                }
                            }

                            target.sendMessage(TextType.PREFIX.getText() + "Du bist nun unsichtbar.");
                            target.playSound(target.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                            player.sendMessage(TextType.PREFIX.getText() + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + " ist nun unsichtbar.");
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                        }
                    } else {
                        player.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                    }
                } else {
                    player.sendMessage(TextType.NO_PERMISSION.getText());
                    return true;
                }
            } else {
                if (args.length == 1) {
                    if (Bukkit.getPlayer(args[0]) == null) {
                        cs.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                        return true;
                    }

                    Player target = Bukkit.getPlayer(args[0]);

                    if (vanish.contains(target)) {
                        vanish.remove(target);

                        for (Player players : Bukkit.getOnlinePlayers()) {
                            players.showPlayer(target);
                        }

                        target.sendMessage(TextType.PREFIX.getText() + "Du bist nun nicht mehr unsichtbar.");
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        cs.sendMessage(TextType.PREFIX.getText() + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + " ist nun nicht mehr unsichtbar.");
                    } else {
                        vanish.add(target);

                        for (Player players : Bukkit.getOnlinePlayers()) {
                            if (!players.hasPermission(Permissions.VANISH_IGNORE.getPermission())) {
                                players.hidePlayer(target);
                            }
                        }

                        target.sendMessage(TextType.PREFIX.getText() + "Du bist nun unsichtbar.");
                        target.playSound(target.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                        target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                        cs.sendMessage(TextType.PREFIX.getText() + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + " ist nun unsichtbar.");
                    }
                } else {
                    cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }
            }
        }

        return true;
    }
}
