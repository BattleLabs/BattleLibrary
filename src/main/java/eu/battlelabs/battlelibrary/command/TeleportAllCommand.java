package eu.battlelabs.battlelibrary.command;

import eu.battlelabs.battlelibrary.constant.Permissions;
import eu.battlelabs.battlelibrary.constant.TextType;
import eu.battlelabs.battlelibrary.objects.data.ServerData;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeleportAllCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("teleportall")) {
            if (cs instanceof Player) {
                Player player = (Player) cs;

                if (!player.hasPermission(Permissions.TELEPORTALL_MAIN.getPermission())) {
                    player.sendMessage(TextType.NO_PERMISSION.getText());
                    player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                    return true;
                }

                for (Player players : Bukkit.getOnlinePlayers()) {
                    players.teleport(player.getLocation());
                    players.playSound(players.getLocation(), Sound.ENDERMAN_TELEPORT, 1, 1);
                }

                Bukkit.broadcastMessage(TextType.PREFIX.getText() + "Es wurden alle zu " + ServerData.playerdatas.get(player).getRank().getColor() + player.getName() + " §7teleportiert.");
                player.playSound(player.getLocation(), Sound.ENDERMAN_TELEPORT, 1, 1);
            } else {
                cs.sendMessage("§cNur für Spieler!");
            }
        }

        return true;
    }
}
