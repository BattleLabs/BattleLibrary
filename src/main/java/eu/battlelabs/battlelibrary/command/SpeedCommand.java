package eu.battlelabs.battlelibrary.command;

import eu.battlelabs.battlelibrary.constant.Permissions;
import eu.battlelabs.battlelibrary.constant.TextType;
import eu.battlelabs.battlelibrary.objects.data.ServerData;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import javax.xml.soap.Text;

public class SpeedCommand implements CommandExecutor {

    private String usage = "/speed <0-10>";

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("speed")) {
            if (cs instanceof Player) {
                Player player = (Player) cs;

                if (player.hasPermission(Permissions.SPEED_MAIN.getPermission())) {
                    if (args.length == 1) {
                        float speed = Float.parseFloat(args[0]);
                        speed = speed / 10;

                        if (speed <= 10 || speed > -1) {
                            if (!player.isFlying()) {
                                player.setWalkSpeed(speed);
                                player.sendMessage(TextType.PREFIX.getText() + "Du hast erfolgreich deine §bLaufgeschwindigkeit §7auf §b" + speed + " §7gesetzt.");
                                player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                            } else {
                                player.setFlySpeed(speed);
                                player.sendMessage(TextType.PREFIX.getText() + "Du hast erfolgreich deine §bFluggeschwindigkeit §7auf §b" + speed + " §7gesetzt.");
                                player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                            }
                        } else {
                            player.sendMessage(TextType.PREFIX.getText() + "§cDie Zahl darf nur kleiner als 10 oder höher als -1 sein.");
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                        }
                    } else if (args.length == 2) {
                        if (!player.hasPermission(Permissions.SPEED_OTHER.getPermission())) {
                            player.sendMessage(TextType.NO_PERMISSION.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        if (Bukkit.getPlayer(args[1]) != null) {
                            player.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        Player target = Bukkit.getPlayer(args[1]);
                        float speed = Float.parseFloat(args[0]);
                        speed = speed / 10;

                        if (speed <= 10 || speed > -1) {
                            if (!target.isFlying()) {
                                target.setWalkSpeed(speed);
                                target.sendMessage(TextType.PREFIX.getText() + "Deine §bLaufgeschwindigkeit §7wurde auf §b" + speed + " §7gesetzt.");
                                target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                                player.sendMessage(TextType.PREFIX.getText() + "Die §bLaufgeschwinigkeit §7von " + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + " §7wurde auf §b" + speed + " §7gesetzt.");
                                player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                            } else {
                                target.setFlySpeed(speed);
                                target.sendMessage(TextType.PREFIX.getText() + "Deine §bFluggeschwindigkeit §7wurde auf §b" + speed + " §7gesetzt.");
                                target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                                player.sendMessage(TextType.PREFIX.getText() + "Die §bLaufgeschwinigkeit §7von " + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + " §7wurde auf §b" + speed + " §7gesetzt.");
                                player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                            }
                        } else {
                            player.sendMessage(TextType.PREFIX.getText() + "§cDie Zahl darf nur kleiner als 10 oder höher als -1 sein.");
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                        }

                    } else {
                        player.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                    }
                } else {
                    player.sendMessage(TextType.NO_PERMISSION.getText());
                    player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                }
            } else {
                if (args.length == 2) {
                    if (Bukkit.getPlayer(args[1]) != null) {
                        cs.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                        return true;
                    }

                    Player target = Bukkit.getPlayer(args[1]);
                    float speed = Float.parseFloat(args[0]);
                    speed = speed / 10;

                    if (speed <= 10 || speed > -1) {
                        if (!target.isFlying()) {
                            target.setWalkSpeed(speed);
                            target.sendMessage(TextType.PREFIX.getText() + "Deine §bLaufgeschwindigkeit §7wurde auf §b" + speed + " §7gesetzt.");
                            target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                            cs.sendMessage(TextType.PREFIX.getText() + "Die §bLaufgeschwinigkeit §7von " + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + " §7wurde auf §b" + speed + " §7gesetzt.");
                        } else {
                            target.setFlySpeed(speed);
                            target.sendMessage(TextType.PREFIX.getText() + "Deine §bFluggeschwindigkeit §7wurde auf §b" + speed + " §7gesetzt.");
                            target.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);

                            cs.sendMessage(TextType.PREFIX.getText() + "Die §bLaufgeschwinigkeit §7von " + ServerData.playerdatas.get(target).getRank().getColor() + target.getName() + " §7wurde auf §b" + speed + " §7gesetzt.");
                        }
                    } else {
                        cs.sendMessage(TextType.PREFIX.getText() + "§cDie Zahl darf nur kleiner als 10 oder höher als -1 sein.");
                    }

                } else {
                    cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }
            }
        }

        return true;
    }
}
