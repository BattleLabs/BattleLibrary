package eu.battlelabs.battlelibrary.command;

import eu.battlelabs.battlelibrary.constant.Permissions;
import eu.battlelabs.battlelibrary.constant.TextType;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class InvseeCommand implements CommandExecutor {

    private String usage = "/invsee {Spieler}";

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("invsee")) {
            if (cs instanceof Player) {
                Player player = (Player)cs;

                if (player.hasPermission(Permissions.INVSEE_MAIN.getPermission())) {
                    if (args.length == 1) {
                        if (Bukkit.getPlayer(args[0]) == null) {
                            player.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                            player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                            return true;
                        }

                        Player target = Bukkit.getPlayer(args[0]);

                        player.openInventory(target.getInventory());
                        player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 1);
                    } else {
                        player.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                    }
                } else {
                    player.sendMessage(TextType.NO_PERMISSION.getText());
                    player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                }
            } else {
                cs.sendMessage("§cNur für Spieler!");
            }
        }

        return true;
    }
}
