package eu.battlelabs.battlelibrary.command;

import eu.battlelabs.battlelibrary.constant.Permissions;
import eu.battlelabs.battlelibrary.constant.TextType;
import eu.battlelabs.battlelibrary.objects.player.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ChatClearCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("chatclear")) {
            if (cs instanceof Player) {
                Player player = (Player)cs;

                if (!player.hasPermission(Permissions.CHATCLEAR_MAIN.getPermission())) {
                    player.sendMessage(TextType.NO_PERMISSION.getText());
                    player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 1, 1);
                    return true;
                }

                new Thread() {
                    @Override
                    public void run() {
                        for (int i = 0; i < 100; i++) {
                            for (Player players : Bukkit.getOnlinePlayers()) {
                                if (!players.hasPermission(Permissions.CHATCLEAR_IGNORE.getPermission())) {
                                    players.sendMessage("");
                                }
                            }
                        }

                        super.run();
                    }
                }.start();

                Bukkit.broadcastMessage("");
                Bukkit.broadcastMessage("");
                Bukkit.broadcastMessage("§8>>      §7Der Globale Chat wurde §bgeleert§7!");
                Bukkit.broadcastMessage("");
                Bukkit.broadcastMessage("");
                Messaging.playSoundForAll(Sound.NOTE_PLING);

            } else {
                new Thread() {
                    @Override
                    public void run() {
                        for (int i = 0; i < 100; i++) {
                            for (Player players : Bukkit.getOnlinePlayers()) {
                                if (!players.hasPermission(Permissions.CHATCLEAR_IGNORE.getPermission())) {
                                    players.sendMessage("");
                                }
                            }
                        }

                        super.run();
                    }
                }.start();

                Bukkit.broadcastMessage("");
                Bukkit.broadcastMessage("");
                Bukkit.broadcastMessage("§8>>      §7Der Globale Chat wurde §bgeleert§7!");
                Bukkit.broadcastMessage("");
                Bukkit.broadcastMessage("");
                Messaging.playSoundForAll(Sound.NOTE_PLING);

            }
        }

        return true;
    }
}
