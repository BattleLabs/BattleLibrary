package eu.battlelabs.battlelibrary.threads;

import eu.battlelabs.battlelibrary.BattleLibrary;
import eu.battlelabs.battlelibrary.objects.data.ServerData;
import eu.battlelabs.battlelibrary.objects.unit.BattleUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.SQLException;

public class UpdaterThread extends BukkitRunnable {

    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            updateStats(player);
        }
    }

    public static void updateStats(Player player) {
        try {
            if (ServerData.playerdatas.containsKey(player)) {
                BattleUser battleUser = ServerData.playerdatas.get(player);

                BattleLibrary.getInstance().getPlayerDataHandler().updateData("tokens", battleUser.getTokens(), player.getUniqueId().toString());
                BattleLibrary.getInstance().getPlayerDataHandler().updateData("votePoints", battleUser.getVotePoints(), player.getUniqueId().toString());
                BattleLibrary.getInstance().getPlayerDataHandler().updateData("kills", battleUser.getKills(), player.getUniqueId().toString());
                BattleLibrary.getInstance().getPlayerDataHandler().updateData("deaths", battleUser.getDeaths(), player.getUniqueId().toString());
                BattleLibrary.getInstance().getPlayerDataHandler().updateData("server", "offline", player.getUniqueId().toString());
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
