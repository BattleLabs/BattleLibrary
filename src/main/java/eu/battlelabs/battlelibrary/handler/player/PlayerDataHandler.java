package eu.battlelabs.battlelibrary.handler.player;

import eu.battlelabs.battlelibrary.BattleLibrary;
import eu.battlelabs.battlelibrary.constant.ObjectType;
import eu.battlelabs.battlelibrary.handler.database.DataLoader;
import eu.battlelabs.battlelibrary.handler.database.MySQLHandler;
import eu.battlelabs.battlelibrary.objects.data.ServerData;
import eu.battlelabs.battlelibrary.objects.unit.BattleBan;
import eu.battlelabs.battlelibrary.objects.unit.BattleUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Diese Klasse verwaltet die Daten der Spieler. Man kann auf sie zugreifen, ändern oder auch löschen.
 * Außerdem kann man Spieler-Daten hinzufügen.
 */
public class PlayerDataHandler extends DataLoader {

    private static String PLAYER_DATA = "battlelabs_battlesky_playerdata";
    private static String SQL_SYNTAX = "id, uuid, currentPlayerName, lastName, lastIp, tokens, votePoints, kills, deaths, banList, server";
    private static String TABLE_SYNTAX = "id int, uuid text, currentPlayerName text, lastName text, lastIp text, tokens int, votePoints int, kills int, deaths int, banList text, server text";

    public PlayerDataHandler(MySQLHandler mySQLHandler) {
        super(mySQLHandler);
    }

    /**
     * Diese Methode ladet die Informationen von einem Spieler.
     * Diese Spieler-Informationen werden durch den Identifikator Player player identifiziert.
     *
     * @param player Der Identifikator für die Player-Informationen
     */
    public void loadData(Player player) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = getMySQLHandler().getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM " + PLAYER_DATA + " WHERE uuid='" + player.getUniqueId().toString() + "'");
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                int id = resultSet.getInt("id");
                UUID uuid = UUID.fromString(resultSet.getString("uuid"));
                String currentPlayername = resultSet.getString("currentPlayerName");
                String lastName = resultSet.getString("lastName");

                if (!currentPlayername.equals(player.getName())) {
                    updateData("currentPlayerName", player.getName(), uuid.toString());
                    updateData("lastName", currentPlayername, uuid.toString());

                    lastName = currentPlayername;
                    currentPlayername = player.getName();
                }

                String lastIP = player.getAddress().getHostString() + ":" + player.getAddress().getPort();
                updateData("lastIp", lastIP, uuid.toString());

                int tokens = resultSet.getInt("tokens");
                int votePoints = resultSet.getInt("votePoints");
                int kills = resultSet.getInt("kills");
                int deaths = resultSet.getInt("deaths");

                String[] banArray = resultSet.getString("banList").split("~/");
                List<BattleBan> banList = new ArrayList<>();

                for (String banId : banArray) {
                    if (ObjectType.isInteger(banId)) {
                        banList.add(BattleLibrary.getInstance().getBanDataHandler().loadData(Integer.valueOf(banId)));
                    }
                }

                updateData("server", Bukkit.getServerName(), uuid.toString());

                ServerData.playerdatas.put(player, new BattleUser(
                        id,
                        uuid,
                        currentPlayername,
                        lastName,
                        lastIP,
                        tokens,
                        votePoints,
                        kills,
                        deaths,
                        banList,
                        ServerData.getRank(player),
                        Bukkit.getServerName()
                ));
            }

            return;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void unloadData(Player player) {
        BattleUser battleUser = ServerData.playerdatas.get(player);

        try {
            updateData("tokens", battleUser.getTokens(), player.getUniqueId().toString());
            updateData("votePoints", battleUser.getVotePoints(), player.getUniqueId().toString());
            updateData("kills", battleUser.getKills(), player.getUniqueId().toString());
            updateData("deaths", battleUser.getDeaths(), player.getUniqueId().toString());
            updateData("server", "offline", player.getUniqueId().toString());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        battleUser = null;
        ServerData.playerdatas.remove(player);
    }

    public void createData(Player player) {
        try {

            ServerData.registered_players++;

            insertData("'" + ServerData.registered_players + "' , '" + player.getUniqueId().toString() + "' , '" + player.getName() + "' , '" + player.getName() + "' , '" + player.getAddress().getHostName() + ":" + player.getAddress().getPort() + "' , '" + 0 + "' , '" + 0 + "' , '" + 0 + "' , '" + 0 + "' , 'none' , '" + Bukkit.getServerName() + "'");
            ServerData.playerdatas.put(player, new BattleUser(
                    ServerData.registered_players,
                    player.getUniqueId(),
                    player.getName(),
                    player.getName(),
                    player.getAddress().getHostString() + ":" + player.getAddress().getPort(),
                    0,
                    0,
                    0,
                    0,
                    new ArrayList<BattleBan>(),
                    ServerData.getRank(player),
                    Bukkit.getServerName()
            ));
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public boolean exists(Player player) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = getMySQLHandler().getConnection();
            preparedStatement = connection.prepareStatement("SELECT id FROM " + PLAYER_DATA + " WHERE uuid=?");
            preparedStatement.setString(1, player.getUniqueId().toString());
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                return resultSet != null;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return false;
    }

    @Override
    public Object getData(ResultSet rs, ObjectType obj, String key) throws SQLException {
        while (rs.next()) {
            switch (obj) {
                case INTEGER:
                    return rs.getInt(key);
                case STRING:
                    return rs.getString(key);
                case BOOLEAN:
                    return rs.getBoolean(key);
                case LONG:
                    return rs.getLong(key);
                case DOUBLE:
                    return rs.getDouble(key);
            }
        }

        return null;
    }

    @Override
    public void updateData(String key, Object value, Object search) throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getMySQLHandler().getConnection();

            preparedStatement = connection.prepareStatement("UPDATE " + PLAYER_DATA + " SET " + key + "=? WHERE uuid=?");
            preparedStatement.setObject(1, value);
            preparedStatement.setObject(2, search);

            preparedStatement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    @Override
    public void dropData(Object search) throws IOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getMySQLHandler().getConnection();

            preparedStatement = connection.prepareStatement("DELETE FROM " + PLAYER_DATA + " WHERE uuid=?");
            preparedStatement.setObject(1, search);

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    @Override
    public void insertData(String value) throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getMySQLHandler().getConnection();

            preparedStatement = connection.prepareStatement("INSERT INTO " + PLAYER_DATA + " (" + SQL_SYNTAX + ") VALUES (" + value + ")");
            preparedStatement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public static void createTable() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = BattleLibrary.getInstance().getMySQLHandler().getConnection();

            preparedStatement = connection.prepareStatement("CREATE TABLE IF NOT EXISTS " + PLAYER_DATA + " (" + TABLE_SYNTAX + ")");
            preparedStatement.execute();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public static void deleteTable() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = BattleLibrary.getInstance().getMySQLHandler().getConnection();

            preparedStatement = connection.prepareStatement("DROP TABLE IF EXISTS " + PLAYER_DATA);
            preparedStatement.execute();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public static void clearTable() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = BattleLibrary.getInstance().getMySQLHandler().getConnection();

            preparedStatement = connection.prepareStatement("TRUNCATE " + PLAYER_DATA);
            preparedStatement.execute();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
