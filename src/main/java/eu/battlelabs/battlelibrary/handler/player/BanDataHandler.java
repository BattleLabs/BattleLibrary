package eu.battlelabs.battlelibrary.handler.player;

import eu.battlelabs.battlelibrary.BattleLibrary;
import eu.battlelabs.battlelibrary.constant.ObjectType;
import eu.battlelabs.battlelibrary.handler.database.DataLoader;
import eu.battlelabs.battlelibrary.handler.database.MySQLHandler;
import eu.battlelabs.battlelibrary.objects.unit.BattleBan;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * Diese Klasse verwaltet die Daten der Ban-Listen. Man kann auf sie zugreifen, ändern oder auch löschen.
 * Außerdem kann man Ban-Einträge hinzufügen.
 */
public class BanDataHandler extends DataLoader {

    // Der Name der MySQL Tabelle
    private static String BAN_DATA = "battlelabs_battlesky_bandata";
    // Der Aufbau der Tabelle
    private static String SQL_SYNTAX = "id, uuid, banDate, banner, reason, untilTime";
    // Der Aufbau der Tabelle mit DatenTyp.
    private static String TABLE_SYNTAX = "id int, uuid text, banDate date, banner text, reason text, untilTime long";

    public BanDataHandler(MySQLHandler mySQLHandler) {
        super(mySQLHandler);
    }

    /**
     * Diese Methode ladet die Informationen von einem Ban-Eintrag und gibt ihn zurück.
     * Dieser Ban-Eintrag wird durch den Identifikator int id identifiziert.
     *
     * @param id Der Identifikator für den Ban-Eintrag.
     * @return Der Ban-Eintrag als Objekt.
     */
    public BattleBan loadData(int id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = getMySQLHandler().getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM " + BAN_DATA + " WHERE id='" + id + "'");
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                return new BattleBan(
                        id,
                        UUID.fromString(resultSet.getString("uuid")),
                        resultSet.getDate("banDate"),
                        resultSet.getString("banner"),
                        resultSet.getString("reason"),
                        resultSet.getLong("untilTime")
                );
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return null;
    }


    @Override
    public Object getData(ResultSet rs, ObjectType obj, String key) throws SQLException {
        while (rs.next()) {
            switch (obj) {
                case INTEGER:
                    return rs.getInt(key);
                case STRING:
                    return rs.getString(key);
                case BOOLEAN:
                    return rs.getBoolean(key);
                case LONG:
                    return rs.getLong(key);
                case DOUBLE:
                    return rs.getDouble(key);
            }
        }
        return null;
    }

    @Override
    public void updateData(String key, Object value, Object search) throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getMySQLHandler().getConnection();

            preparedStatement = connection.prepareStatement("UPDATE " + BAN_DATA + " SET " + key + "=? WHERE uuid=?");
            preparedStatement.setObject(1, value);
            preparedStatement.setObject(2, search);

            preparedStatement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    @Override
    public void insertData(String value) throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getMySQLHandler().getConnection();

            preparedStatement = connection.prepareStatement("INSERT INTO " + BAN_DATA + " (" + SQL_SYNTAX + ") VALUES (" + value + ")");
            preparedStatement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    @Override
    public void dropData(Object search) throws IOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getMySQLHandler().getConnection();

            preparedStatement = connection.prepareStatement("DELETE FROM " + BAN_DATA + " WHERE uuid=?");
            preparedStatement.setObject(1, search);

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public static void createTable() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = BattleLibrary.getInstance().getMySQLHandler().getConnection();

            preparedStatement = connection.prepareStatement("CREATE TABLE IF NOT EXISTS " + BAN_DATA + " (" + TABLE_SYNTAX + ")");
            preparedStatement.execute();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public static void deleteTable() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = BattleLibrary.getInstance().getMySQLHandler().getConnection();

            preparedStatement = connection.prepareStatement("DROP TABLE IF EXISTS " + BAN_DATA);
            preparedStatement.execute();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public static void clearTable() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = BattleLibrary.getInstance().getMySQLHandler().getConnection();

            preparedStatement = connection.prepareStatement("TRUNCATE " + BAN_DATA);
            preparedStatement.execute();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

}
