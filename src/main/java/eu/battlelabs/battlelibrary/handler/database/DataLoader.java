package eu.battlelabs.battlelibrary.handler.database;

import eu.battlelabs.battlelibrary.constant.ObjectType;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class DataLoader {

    private MySQLHandler mySQLHandler;

    public DataLoader(MySQLHandler mySQLHandler) {
        this.mySQLHandler = mySQLHandler;
    }

    public abstract Object getData(ResultSet rs, ObjectType obj, String key) throws SQLException;

    public abstract void updateData(String key, Object value, Object search) throws SQLException;

    public abstract void dropData(Object search) throws IOException;

    public abstract void insertData(String value) throws SQLException;

    public MySQLHandler getMySQLHandler() {
        return mySQLHandler;
    }

}
