package eu.battlelabs.battlelibrary.handler.client;

import eu.battlelabs.battlelibrary.BattleLibrary;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.bukkit.Bukkit;

public class NetworkHandler extends SimpleChannelInboundHandler<ByteBuf> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
        String[] args = BattleLibrary.getInstance().getNettyClient().readString(msg).split(" ");
        String cmd = args[0].toLowerCase();

        if (cmd.equalsIgnoreCase("end")) {
            Bukkit.shutdown();
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
    }
}
