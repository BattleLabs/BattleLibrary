package eu.battlelabs.battlelibrary.handler.client;

import com.google.common.base.Charsets;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.epoll.Epoll;

/**
 * Diese Klasse besitzt Informationen über den Netty Client und kann auch Strings zum Server senden,
 * oder Nachrichten vom Server lesen.
 */
public abstract class ClientHandler {

    // Der Host des Netty Servers
    private String host;
    // Der Port des Netty Servers
    private int port;

    public ClientHandler(int port) {
        this("127.0.0.1", port);
    }

    public ClientHandler(String host, int port) {
        this.host = host; this.port = port;
    }

    /**
     * Diese Methode stellt die Verbindung zum Server her. Wenn es schief geht, wird eine
     * Nachricht versendet.
     */
    public abstract void setupServerConnection();

    /**
     * Gibt den Host des Servers als String zurück.
     *
     * @return Host des Servers
     */
    public String getHost() {
        return host;
    }

    /**
     * Gibt den Port des Servers als int zurück.
     *
     * @return Port des Servers
     */
    public int getPort() {
        return port;
    }

    public boolean epollAvailable() {
        return Epoll.isAvailable();
    }

    /**
     * Vearbeitet den angekommenen ByteBuf in einen String, sodass man
     * die Nachricht lesen kann.
     *
     * @param buf Die Nachricht, die binär ist und in einen String gemacht wird.
     * @return Die Nachricht des Servers
     */
    public String readString(ByteBuf buf) {
        buf.readInt();
        int length = buf.readInt();
        byte[] byteArray = new byte[length];
        buf.readBytes(byteArray);
        buf.resetReaderIndex();

        return new String(byteArray, Charsets.UTF_8);
    }

    /**
     * Vearbeitet den String in einen ByteBuf und schickt es zum Server.
     *
     * @param value Die Nachricht, die zum ByteBuf vearbeitet wird.
     * @param channel Der Server-Channel, der die Nachricht empfängt.
     */
    public void sendString(String value, Channel channel) {
        ByteBuf buf = Unpooled.buffer();
        byte[] byteArray = value.getBytes(Charsets.UTF_8);
        buf.writeInt(byteArray.length);
        buf.writeBytes(byteArray);
        channel.writeAndFlush(buf);
    }

}
