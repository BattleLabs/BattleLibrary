package eu.battlelabs.battlelibrary.handler.source;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

public class SQLConfigHandler {

    private File file;
    private FileConfiguration configuration;

    public SQLConfigHandler() {
        this.file = new File("plugins/BattleLabs/", "mysql.yml");
        this.configuration = YamlConfiguration.loadConfiguration(this.file);

        if (!getFile().exists()) {
            getConfiguration().addDefault("MySQL." + "host", "host");
            getConfiguration().addDefault("MySQL." + "port", 3306);
            getConfiguration().addDefault("MySQL." + "username", "root");
            getConfiguration().addDefault("MySQL." + "password", "1234");
            getConfiguration().options().copyDefaults(true);
            save();
        }
    }

    public File getFile() {
        return file;
    }

    public FileConfiguration getConfiguration() {
        return configuration;
    }

    public void save() {
        try {
            getConfiguration().save(getFile());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
