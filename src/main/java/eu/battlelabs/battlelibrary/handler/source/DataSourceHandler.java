package eu.battlelabs.battlelibrary.handler.source;

public abstract class DataSourceHandler {

    private String database = "battlelabs";

    public abstract String getHost();

    public abstract int getPort();

    public abstract String getUsername();

    public abstract String getPassword();

    public String getDatabase() {
        return database;
    }

}
