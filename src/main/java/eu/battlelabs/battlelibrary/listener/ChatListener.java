package eu.battlelabs.battlelibrary.listener;

import eu.battlelabs.battlelibrary.BattleLibrary;
import eu.battlelabs.battlelibrary.constant.BattleRank;
import eu.battlelabs.battlelibrary.constant.Permissions;
import eu.battlelabs.battlelibrary.constant.TextType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {

        Player player = e.getPlayer();

        if (player.hasPermission(BattleRank.ADMIN.getPermission())) {
            e.setFormat("§4§lAdmin §8▌ §4§l" + player.getName() + " §8» §7" + e.getMessage());
        } else if (player.hasPermission(BattleRank.DEVELOPER.getPermission())) {
            e.setFormat("§3§oDeveloper §8▌ §3§l" + player.getName() + " §8» §7" + e.getMessage());
        } else if (player.hasPermission(BattleRank.SRMOD.getPermission())) {
            e.setFormat("§cSr.Mod §8▌ §c" + player.getName() + " §8» §7" + e.getMessage());
        } else if (player.hasPermission(BattleRank.MODERATOR.getPermission())) {
            e.setFormat("§eModerator §8▌ §e" + player.getName() + " §8» §7" + e.getMessage());
        } else if (player.hasPermission(BattleRank.SUPPORTER.getPermission())) {
            e.setFormat("§bSupporter §8▌ §b" + player.getName() + " §8» §7" + e.getMessage());
        } else if (player.hasPermission(BattleRank.BUILDER.getPermission())) {
            e.setFormat("§2" + player.getName() + " §8» §7" + e.getMessage());
        } else if (player.hasPermission(BattleRank.YOUTUBER.getPermission())) {
            e.setFormat("§5" + player.getName() + " §8» §7" + e.getMessage());
        } else if (player.hasPermission(BattleRank.PREMIUMP.getPermission())) {
            e.setFormat("§6§l" + player.getName() + " §8» §7" + e.getMessage());
        } else if (player.hasPermission(BattleRank.PREMIUM.getPermission())) {
            e.setFormat("§6" + player.getName() + " §8» §7" + e.getMessage());
        } else {
            e.setFormat("§9" + player.getName() + " §8» §7" + e.getMessage());
        }

        if (!BattleLibrary.chat) {
            if (!player.hasPermission(Permissions.GLOBALMUTE_BYPASS.getPermission())) {
                e.setCancelled(true);
                player.sendMessage(TextType.PREFIX.getText() + "§cDu kannst nicht schreiben, da der Chat deaktiviert ist.");
            }
        }
    }

}
