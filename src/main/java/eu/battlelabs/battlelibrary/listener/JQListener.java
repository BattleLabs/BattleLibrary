package eu.battlelabs.battlelibrary.listener;

import eu.battlelabs.battlelibrary.BattleLibrary;
import eu.battlelabs.battlelibrary.command.VanishCommand;
import eu.battlelabs.battlelibrary.constant.Permissions;
import eu.battlelabs.battlelibrary.constant.TextType;
import eu.battlelabs.battlelibrary.objects.data.ServerData;
import eu.battlelabs.battlelibrary.objects.player.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class JQListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();

        new Thread() {
            @Override
            public void run() {
                if (BattleLibrary.getInstance().getPlayerDataHandler().exists(player)) {
                    BattleLibrary.getInstance().getPlayerDataHandler().loadData(player);
                } else {
                    BattleLibrary.getInstance().getPlayerDataHandler().createData(player);
                }

                super.run();
            }
        }.start();

        for (Player vanishPlayers : VanishCommand.vanish) {
            if (!player.hasPermission(Permissions.VANISH_IGNORE.getPermission())) {
                player.hidePlayer(vanishPlayers);
            }
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();

        new Thread() {
            @Override
            public void run() {
                if (ServerData.playerdatas.containsKey(player)) {
                    BattleLibrary.getInstance().getPlayerDataHandler().unloadData(player);
                } else {
                    Messaging.sendMessageForHasPermission(TextType.CMD_PREFIX.getText() + "Der Spieler §e" + player.getName() + " §cbesitzte keine §eSpielerdaten§c.", "battlelabs.cmd");
                }

                super.run();
            }
        }.start();
    }

}
