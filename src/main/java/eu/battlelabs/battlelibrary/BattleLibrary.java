package eu.battlelabs.battlelibrary;

import eu.battlelabs.battlelibrary.command.*;
import eu.battlelabs.battlelibrary.handler.database.MySQLHandler;
import eu.battlelabs.battlelibrary.handler.player.BanDataHandler;
import eu.battlelabs.battlelibrary.handler.player.PlayerDataHandler;
import eu.battlelabs.battlelibrary.handler.source.SQLConfigHandler;
import eu.battlelabs.battlelibrary.listener.JQListener;
import eu.battlelabs.battlelibrary.objects.client.NettyClient;
import eu.battlelabs.battlelibrary.threads.UpdaterThread;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class BattleLibrary extends JavaPlugin implements LibraryInterface {

    private static BattleLibrary instance;
    private NettyClient nettyClient;
    private SQLConfigHandler sqlConfigHandler;
    private MySQLHandler mysqlHandler;
    private PlayerDataHandler playerDataHandler;
    private BanDataHandler banDataHandler;
    private PluginManager pluginManager;
    public static boolean chat = true;

    @Override
    public void onEnable() {
        instance = this;

        pluginManager = Bukkit.getPluginManager();
        sqlConfigHandler = new SQLConfigHandler();
        mysqlHandler = new MySQLHandler(getSQLConfigHandler().getConfiguration());
        banDataHandler = new BanDataHandler(getMySQLHandler());
        playerDataHandler = new PlayerDataHandler(getMySQLHandler());

        getBanDataHandler().createTable();
        getPlayerDataHandler().createTable();

        new Thread() {
            @Override
            public void run() {

                nettyClient = new NettyClient(8000);
                getNettyClient().setupServerConnection();

                super.run();
            }
        }.start();

        registerListeners();
        registerCommands();

        new UpdaterThread().runTaskTimerAsynchronously(this, 20L * 60 * 5, 20L * 60 * 5);

        super.onEnable();
    }

    @Override
    public void onDisable() {

        for (Player player : Bukkit.getOnlinePlayers()) {
            UpdaterThread.updateStats(player);
        }

        instance = null;
        nettyClient = null;
        sqlConfigHandler = null;
        mysqlHandler = null;
        playerDataHandler = null;
        banDataHandler = null;
        pluginManager = null;

        super.onDisable();
    }

    public static BattleLibrary getInstance() {
        return instance;
    }

    public NettyClient getNettyClient() {
        return nettyClient;
    }

    public SQLConfigHandler getSQLConfigHandler() {
        return sqlConfigHandler;
    }

    public MySQLHandler getMySQLHandler() {
        return mysqlHandler;
    }

    public PlayerDataHandler getPlayerDataHandler() {
        return playerDataHandler;
    }

    public BanDataHandler getBanDataHandler() {
        return banDataHandler;
    }

    public PluginManager getPluginManager() {
        return pluginManager;
    }

    @Override
    public void registerCommands() {
        getCommand("gamemode").setExecutor(new GamemodeCommand());
        getCommand("chatclear").setExecutor(new ChatClearCommand());
        getCommand("clear").setExecutor(new ClearCommand());
        getCommand("fly").setExecutor(new FlyCommand());
        getCommand("food").setExecutor(new FoodCommand());
        getCommand("globalmute").setExecutor(new GlobalmuteCommand());
        getCommand("heal").setExecutor(new HealCommand());
        getCommand("speed").setExecutor(new SpeedCommand());
        getCommand("god").setExecutor(new GodCommand());
        getCommand("vanish").setExecutor(new VanishCommand());
        getCommand("tokens").setExecutor(new TokenCommand());
        getCommand("votepoints").setExecutor(new VotePointCommand());
        getCommand("broadcast").setExecutor(new BroadcastCommand());
        getCommand("msg").setExecutor(new MsgCommand());
        getCommand("re").setExecutor(new MsgCommand());
        getCommand("socialspy").setExecutor(new MsgCommand());
        getCommand("invsee").setExecutor(new InvseeCommand());
        getCommand("enderchest").setExecutor(new EnderchestCommand());
        getCommand("random").setExecutor(new RandomCommand());
        getCommand("teleportall").setExecutor(new TeleportAllCommand());
        getCommand("workbench").setExecutor(new WorkbenchCommand());
        getCommand("teleport").setExecutor(new TeleportCommand());
    }

    @Override
    public void registerListeners() {
        getPluginManager().registerEvents(new JQListener(), this);
        getPluginManager().registerEvents(new GodCommand(), this);
    }
}
