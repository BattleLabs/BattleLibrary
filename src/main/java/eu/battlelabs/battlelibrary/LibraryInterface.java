package eu.battlelabs.battlelibrary;

import org.bukkit.command.CommandExecutor;
import org.bukkit.event.Event;

public interface LibraryInterface {

    void registerListeners();
    void registerCommands();

}
