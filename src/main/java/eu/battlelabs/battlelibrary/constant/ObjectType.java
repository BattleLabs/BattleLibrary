package eu.battlelabs.battlelibrary.constant;

/**
 * Diese Enum-Klasse stellt alle Objekte da um beim MySQL getData das richtige Objekt zu kriegen.
 */
public enum ObjectType {

    INTEGER,
    STRING,
    BOOLEAN,
    LONG,
    DOUBLE,
    DATE;

    public static boolean isInteger(String str) {
        try {
            Integer.valueOf(str);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

}
