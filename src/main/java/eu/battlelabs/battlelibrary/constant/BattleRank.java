package eu.battlelabs.battlelibrary.constant;

/**
 * Diese Enum-Klasse stellt alle Ränge vom BattleLabsEU Server dar. Man kann durch diese Klasse Informationen,
 * wie die Permissions und den Prefix, kriegen.
 */
public enum BattleRank {

    ADMIN("§4Admin §8▌ §4", "battlelabs.admin", "§4Admin", "§4", "001"),
    DEVELOPER("§3§oDev §8▌ §3§o", "battlelabs.developer", "§3§oDeveloper", "§3§o", "002"),
    SRMOD("§cSr.Mod §8▌ §c", "battlelabs.srmod", "§c§lSr.Mod", "§c§l", "003"),
    MODERATOR("§eMod §8▌ §e", "battlelabs.mod", "§eModerator", "§e", "004"),
    SUPPORTER("§bSupp §8▌ §b", "battlelabs.supporter", "§bSupporter", "§b", "005"),
    BUILDER("§2", "battlelabs.builder", "§2Builder", "§2", "006"),
    YOUTUBER("§5§l", "battlelabs.youtuber", "§5§lYouTuber", "§5§l", "007"),
    PREMIUMP("§6§l", "battlelabs.premiump", "§6§lPremium+", "§6§l", "008"),
    PREMIUM("§6", "battlelabs.premium", "§6Premium", "§6", "009"),
    USER("§9", null, "§9Spieler", "§9", "010");

    private String prefix;
    private String permission;
    private String rank;
    private String color;
    private String order;

    BattleRank(String prefix, String permission, String rank, String color, String order) {
        this.prefix = prefix;
        this.permission = permission;
        this.rank = rank;
        this.color = color;
        this.order = order;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getPermission() {
        return permission;
    }

    public String getRank() {
        return rank;
    }

    public String getColor() {
        return color;
    }

    public String getOrder() {
        return order;
    }
}
