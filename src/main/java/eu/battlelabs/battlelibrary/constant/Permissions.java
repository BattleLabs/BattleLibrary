package eu.battlelabs.battlelibrary.constant;

public enum Permissions {

    GAMEMODE_MAIN("battlelabs.gamemode"),
    GAMEMODE_OTHER("battlelabs.gamemode.other"),
    CHATCLEAR_MAIN("battlelabs.chatclear"),
    CHATCLEAR_IGNORE("battlelabs.chatclear.ignore"),
    CLEAR_MAIN("battlelabs.clear"),
    CLEAR_OTHER("battlelabs.clear.other"),
    FLY_MAIN("battlelabs.fly"),
    FLY_OTHER("battlelabs.fly.other"),
    FOOD_MAIN("battlelabs.food"),
    FOOD_OTHER("battlelabs.food.other"),
    GLOBALMUTE_MAIN("battlelabs.globalmute"),
    GLOBALMUTE_TOGGLE("battlelabs.globalmute.toggle"),
    GLOBALMUTE_BYPASS("battlelabs.globalmute.bypass"),
    HEAL_MAIN("battlelabs.heal"),
    HEAL_OTHER("battlelabs.heal.other"),
    SPEED_MAIN("battlelabs.speed"),
    SPEED_OTHER("battlelabs.speed.other"),
    GOD_MAIN("battlelabs.god"),
    GOD_OTHER("battlelabs.god.other"),
    GOD_CANCEL("battlelabs.god.cancel"),
    VANISH_MAIN("battlelabs.vanish"),
    VANISH_OTHER("battlelabs.vanish.other"),
    VANISH_IGNORE("battlelabs.vanish.ignore"),
    TOKENS_ADD("battlelabs.tokens.add"),
    TOKENS_SET("battlelabs.tokens.set"),
    TOKENS_REMOVE("battlelabs.tokens.remove"),
    VOTEPOINT_ADD("battlelabs.votepoints.add"),
    VOTEPOINT_SET("battlelabs.votepoints.set"),
    VOTEPOINT_REMOVE("battlelabs.votepoints.remove"),
    BROADCAST_MAIN("battlelabs.broadcast"),
    SOCIALSPY_MAIN("battlelabs.socialspy"),
    SOCIALSPY_OTHER("battlelabs.socialspy.other"),
    INVSEE_MAIN("battlelabs.invsee"),
    ENDERCHEST_MAIN("battlelabs.enderchest"),
    RANDOM_MAIN("battlelabs.random"),
    TELEPORTALL_MAIN("battlelabs.teleportall"),
    WORKBECNH_MAIN("battlelabs.workbench"),
    TELEPORT_MAIN("battlelabs.teleport"),
    TELEPORT_OTHER("battlelabs.teleport.other"),
    TELEPORT_COORD("battlelabs.teleport.coord");

    private String permission;

    Permissions(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }

}
