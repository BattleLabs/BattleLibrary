package eu.battlelabs.battlelibrary.objects.data;

import eu.battlelabs.battlelibrary.constant.BattleRank;
import eu.battlelabs.battlelibrary.objects.unit.BattleBan;
import eu.battlelabs.battlelibrary.objects.unit.BattleUser;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class ServerData {

    public static int registered_players = 0;
    public static HashMap<Player, BattleUser> playerdatas = new HashMap<>();
    public static ArrayList<BattleBan> playerbans = new ArrayList<BattleBan>();
    public static Date startDate = null;

    public static BattleRank getRank(Player player) {
        if (player.hasPermission(BattleRank.ADMIN.getPermission())) return BattleRank.ADMIN;
        if (player.hasPermission(BattleRank.DEVELOPER.getPermission())) return BattleRank.DEVELOPER;
        if (player.hasPermission(BattleRank.SRMOD.getPermission())) return BattleRank.SRMOD;
        if (player.hasPermission(BattleRank.MODERATOR.getPermission())) return BattleRank.MODERATOR;
        if (player.hasPermission(BattleRank.SUPPORTER.getPermission())) return BattleRank.SUPPORTER;
        if (player.hasPermission(BattleRank.BUILDER.getPermission())) return BattleRank.BUILDER;
        if (player.hasPermission(BattleRank.YOUTUBER.getPermission())) return BattleRank.YOUTUBER;
        if (player.hasPermission(BattleRank.PREMIUMP.getPermission())) return BattleRank.PREMIUMP;
        if (player.hasPermission(BattleRank.PREMIUM.getPermission())) return BattleRank.PREMIUM;
        return BattleRank.USER;
    }

}
