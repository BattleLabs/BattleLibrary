package eu.battlelabs.battlelibrary.objects.unit;

import eu.battlelabs.battlelibrary.constant.BattleRank;
import org.bukkit.BanList;

import java.text.DecimalFormat;
import java.util.List;
import java.util.UUID;

public class BattleUser {

    private final int id;
    private final UUID uuid;
    private final String currentPlayerName;
    private final String lastName;
    private final String lastIP;
    private int tokens;
    private int votePoints;
    private int kills;
    private int deaths;
    private final List<BattleBan> banList;
    private BattleRank rank;
    private BattleBan lastBan;
    private BattleBan activeBan;
    private String server;

    public BattleUser(BattleUser battleUser) {
        this.id = battleUser.getId();
        this.uuid = battleUser.getUUID();
        this.currentPlayerName = battleUser.getCurrentPlayerName();
        this.lastName = battleUser.getLastName();
        this.lastIP = battleUser.getLastIP();
        this.tokens = battleUser.getTokens();
        this.votePoints = battleUser.getVotePoints();
        this.kills = battleUser.getKills();
        this.deaths = battleUser.getDeaths();
        this.banList = battleUser.getBanList();
        this.rank = battleUser.getRank();
        this.lastBan = battleUser.getLastBan();
        this.activeBan = battleUser.getActiveBan();
        this.server = battleUser.getServer();
    }

    public BattleUser(int id, UUID uuid, String currentPlayerName, String lastName, String lastIP, int tokens, int votePoints, int kills, int deaths, List<BattleBan> banList, BattleRank rank, String server) {
        this.id = id;
        this.uuid = uuid;
        this.currentPlayerName = currentPlayerName;
        this.lastName = lastName;
        this.lastIP = lastIP;
        this.tokens = tokens;
        this.votePoints = votePoints;
        this.kills = kills;
        this.deaths = deaths;
        this.banList = banList;
        this.rank = rank;
        this.lastBan = getBanList().isEmpty() ? null : getBanList().get(getBanList().size() - 1);
        this.server = server;
    }

    public int getId() {
        return id;
    }

    public UUID getUUID() {
        return uuid;
    }

    public String getCurrentPlayerName() {
        return currentPlayerName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getLastIP() {
        return lastIP;
    }

    public int getTokens() {
        return tokens;
    }

    public void setTokens(int tokens) {
        this.tokens = tokens;
    }

    public void addTokens(int amount) {
        setTokens(getTokens() + amount);
    }

    public void removeTokens(int amount) {
        setTokens(getTokens() - amount);
    }

    public int getVotePoints() {
        return votePoints;
    }

    public void setVotePoints(int votePoints) {
        this.votePoints = votePoints;
    }

    public void addVotePoints(int amount) {
        setVotePoints(getVotePoints() + amount);
    }

    public void removeVotePoints(int amount) {
        setVotePoints(getVotePoints() - amount);
    }

    public int getKills() {
        return kills;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    public int getDeaths() {
        return deaths;
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    public double getKDR() {
        double kdr = 0.0D;
        int ki = getKills();
        int de = getDeaths();
        if (de == 0) {
            de = 1;
        }
        kdr = Double.parseDouble(Integer.toString(ki)) / Double.parseDouble(Integer.toString(de));
        DecimalFormat df = new DecimalFormat("0.00");
        String s = df.format(kdr).replace(",", ".");
        if (s.endsWith("0")) {
            s = s.substring(0, s.length() - 1);
        }
        if (s.endsWith("0")) {
            s = s.substring(0, s.length() - 1);
        }
        if (s.endsWith("0")) {
            s = s.substring(0, s.length() - 1);
        }
        kdr = Double.parseDouble(s);
        return kdr;
    }

    public List<BattleBan> getBanList() {
        return banList;
    }

    public BattleRank getRank() {
        return rank;
    }

    public void setRank(BattleRank rank) {
        this.rank = rank;
    }

    public BattleBan getLastBan() {
        return lastBan;
    }

    public void setLastBan(BattleBan lastBan) {
        this.lastBan = lastBan;
    }

    public BattleBan getActiveBan() {
        return activeBan;
    }

    public void setActiveBan(BattleBan activeBan) {
        this.activeBan = activeBan;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }
}
