package eu.battlelabs.battlelibrary.objects.unit;

import java.sql.Date;
import java.util.UUID;

public class BattleBan {

    private final int id;
    private final UUID battleUser;
    private final Date banDate;
    private final String by;
    private final String reason;
    private boolean permanently;
    private long until;

    public BattleBan(int id, UUID battleUser, Date banDate, String by, String reason, long until) {
        this.id = id;
        this.battleUser = battleUser;
        this.banDate = banDate;
        this.by = by;
        this.reason = reason;
        this.until = until;
        this.permanently = until == -1 ? true : false;
    }

    public int getId() {
        return id;
    }

    public UUID getBattleUser() {
        return battleUser;
    }

    public Date getBanDate() {
        return banDate;
    }

    public String getBy() {
        return by;
    }

    public String getReason() {
        return reason;
    }

    public boolean isPermanently() {
        return permanently;
    }

    public long getUntil() {
        return until;
    }
}
