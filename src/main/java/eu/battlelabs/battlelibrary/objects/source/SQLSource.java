package eu.battlelabs.battlelibrary.objects.source;

import eu.battlelabs.battlelibrary.handler.source.DataSourceHandler;

public class SQLSource extends DataSourceHandler {

    private String host;
    private int port;
    private String username;
    private String password;

    public SQLSource(String host, int port, String username, String password) {
        this.host = host; this.port = port; this.username = username; this.password = password;
    }

    @Override
    public String getHost() {
        return host;
    }

    @Override
    public int getPort() {
        return port;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getDatabase() {
        return super.getDatabase();
    }
}
